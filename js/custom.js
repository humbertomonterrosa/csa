function set_val(arr){
	for(i in arr){
		$("form *[name='"+i+"']").val(arr[i]);
	}
}

var Notify = {
				open:200,
				close:200,
				delay:3000,
				show:function(msg){
					$("<div class=\"notify\">"+msg+"</div>").appendTo($(document.body)).HCenter().css("visibility","visible").css("display","none").fadeIn(Notify.open,function(){obj = this; setTimeout(function(){$(obj).fadeOut(Notify.close,function(){$(this).remove()})},Notify.delay)});
				}
			};

$(document).on('ready',function(){
	$("#btn_ps").click(function(){
		var target = $(this).attr("data-target");
		var t = $("#"+target);
		if(t.val()=="P"){ 
			t.val("G"); $(this).html("Garantia"); $("#abono").val(""); $("#abono").attr("disabled","disabled");
		}
		else{ 
			t.val("P"); $(this).html("Particular"); $("#abono").removeAttr("disabled"); 
		}
	});

	$(".btn_multival").each(function(){
		var target = $(this).attr("data-target");
		var values = $(this).attr("data-values");
		var index  = $(this).attr("data-index")||0;
		var vals = values.split("|");
		$(this).html(vals[index].split(":")[0]+' <span class="icon-hand-up icon-white"></span>');
		$(target).val(vals[index].split(":")[1]);
		$(this).attr("data-index",index);

	});

	$(".btn_multival").click(function(){
		var target = $(this).attr("data-target");
		var values = $(this).attr("data-values");
		var index =	$(this).attr("data-index");
		var vals = values.split("|");
		var max=vals.length;
		if(index<max-1){
			index++;
		}
		else{
			index=0;
		}
		$(this).html(vals[index].split(":")[0]+' <span class="icon-hand-up icon-white"></span>');
		$(target).val(vals[index].split(":")[1]);
		$(this).attr("data-index",index);

	});

	$.Shortcuts.add({
	    type: 'down',
	    mask: 'Ctrl+B',
	    handler: function() {
	        $("#buscarods").focus();
	    }
	});
	
	$.Shortcuts.add({
	    type: 'up',
	    mask: '.+H',
	    handler: function() {
	        location.href=base_url;
	    }
	});

	$.Shortcuts.add({
	    type: 'up',
	    mask: '.+N',
	    handler: function() {
	        location.href=base_url+"ods/nueva";
	    }
	});

	$.Shortcuts.add({
	    type: 'up',
	    mask: '.+L',
	    handler: function() {
	        location.href=base_url+"ods/";
	    }
	});

	$.Shortcuts.start();

	$("*[rel='tooltip']").tooltip({html:true});
	$("*[rel='popover']").popover({html:true});

	$("a[rel='go']").click(function(){
		var go = $(this).attr('data-go');
		history.go(go);
		return false;
	});	

	$("form.confirm-form").on('submit',function(){
		var msg = $(this).attr("data-msg");
		return confirm(msg);
	});

	$(".confirm-seleccion").on('click',function(){
		var seleccion = $(this).attr("data-item");
		return confirm("Usted seleccionó \""+seleccion+"\"\n¿Desea continuar el proceso con esta selección?");
	});

	$(".link-confirm").click(function(){
		var msg = $(this).attr("data-msg");
		return confirm(msg);
	});

	$(".form-ajax").on('submit',function(){
		obj = $(this);
		action = obj.attr("action");
		data = obj.serialize();
		method = obj.attr("method");
		callbacks = {
						init:false,
						success:false,
						error:false,
						finalize:false
					};

		callbacks.init = eval(obj.attr("data-init"))||(function(){});
		callbacks.success = eval(obj.attr("data-success"))||(function(){});
		callbacks.error = eval(obj.attr("data-error"))||(function(){});
		callbacks.finalize = eval(obj.attr("data-finalize"))||(function(){});

		callbacks.init();

		$.ajax({
			url:action,
			type: method,
			data:data,
			dataType:'json',
			success:function(a){
				callbacks.success(a);
			},
			error:function(a,b,c){
				callbacks.error(a,b,c);
			},
			complete:function(){
				callbacks.finalize();
			}
		});

		return false;
	});

	$(".dropdown-toggle").dropdown();

	$("form").attr("autocomplete","off");

	$("#lista-tabla").dataTable();

	$(".img-producto>a").each(function(i,e){
		$(this).css("background-image","url("+$(this).attr("data-img")+")")
	});

	$(".img-producto>a.imagen").fancybox({
		helpers:{title:null}
	});

});

jQuery.fn.HCenter = function () {
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
}

function setServicio(tipo,abono){
	if(tipo=="G"){
		$("#tiposervicio").val("G");
		$("#btn_ps").html("Garantia");
		$("#abono").val("");
		$("#abono").attr("disabled","disabled");
	}
	else if(tipo=="P"){
		$("#tiposervicio").val("P");
		$("#btn_ps").html("Particular");
		$("#abono").val(abono);
	}
}

