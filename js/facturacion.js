__PZ_VT = 0;
__PZ_CT = 0;
__TOTAL = 0;
__TRABAJO = 0;
__TOTALNETO = "totalneto";
__PZ = 1;
function agregarPieza(idContainer,pieza,cantidad,valor,idpzu,mod,target){
	pieza = pieza||'';
	cantidad = cantidad||1;
	valor = valor||0;
	idpzu=idpzu||'void';
	mod = (mod==null?true:mod);
	target = target||null;
	var obj = '<div class="row-fluid __PZ" id="__PZ'+__PZ+'">'+
			  '<div class="span7"><input id="__PZ'+__PZ+'_" type="text" name="pieza[]" class="fill_parent no-radius piezaselector" autofocus="autofocus" value="'+pieza+'"'+(!mod?' readonly="readonly"':'')+'></div>'+
			  '<div class="span2"><input id="__PZ'+__PZ+'_C" type="text" name="cantidad[]" class="fill_parent no-radius text-center integer" value="'+cantidad+'"'+(!mod&&target=='all'?' readonly="readonly"':'')+'></div>'+
			  '<div class="span3"><input id="__PZ'+__PZ+'_V" type="text" name="precio[]" class="fill_parent no-radius text-right integer" value="'+valor+'"'+(!mod&&target=='all'?' readonly="readonly"':'')+'>'+(!mod&&target=='all'?'':'<a href="#" class="btn btn-danger btn-mini" data-idpzu="'+idpzu+'"><span class="icon-remove icon-white"></span></a>')+'</div>'+
			  '</div>';
	
	$(obj).appendTo($("#"+idContainer));
	if(mod){
	$("#__PZ"+__PZ+"_").typeahead({
							source:function(typeahead,query){
								return $.ajax({url:base_url+"suggest/pieza",type:"POST",dataType:"json",data:"pieza="+typeahead,success:function(data){return query(data);}});
							},
							updater:function(item){
								__PZ_ = $(this.$element).attr("id");
								var part = item.split(" - $ ");
								var pz = part[0];
								$("#"+__PZ_+"C").val('1');
								$("#"+__PZ_+"V").val(part[1]).focus();
								return pz;
							}
						}
		);
		}
	var items = $("input[name='pieza[]']");
	$(items[items.length-1]).focus();
	__PZ++;
}

function calcularNeto(target){
	total = 0;
	$(".__PZ").each(function(i){
		id = $(this).attr("id");
		c = parseInt($("#"+id+"_C").val())||0;
		v = parseInt($("#"+id+"_V").val())||0;
		total += c*v;
	});
	$("#totalpiezas").val(total);
	totaltrabajo = parseInt($("#totaltrabajo").val())||0
	descuento = parseInt($("#descuento").val())||0;
	total += totaltrabajo;
	total -= descuento;
	$("#"+target).html("$ "+total);

}

function unirDuplicados(){
	arr = [];
	$(".__PZ").each(function(i){
		id = $(this).attr("id");
		pieza = $("#"+id+"_");
		cantidad = $("#"+id+"_C");
		valor = $("#"+id+"_V");
		obj = {'index':i,'objeto':$(this),'id':id,'pieza':pieza,'cantidad':cantidad,'valor':valor};
		arr.push(obj);
	});

	for(i=0;i<arr.length;i++){
		for(x=0;x<arr.length;x++){
			if(x!=i){
				if(($(arr[i].pieza).val()==$(arr[x].pieza).val())){
					$(arr[i].cantidad).val(parseInt($(arr[i].cantidad).val())+parseInt($(arr[x].cantidad).val()));
					$(arr[x].objeto).remove();
					$(".pz .__PZ:last-child [class^='span']:last-child input[type='text']").focus();
					arr.splice(x,1);
					
				}
			}
		}
	}
}

$(document).on('ready',function(){

	$(".add-pz").click(function(){
		cpz = $(this).attr("data-content-pz");
		agregarPieza(cpz);
		return false;
	});

	$("input[name='precio[]']").live("keyup",function(e){
		
		return false;
	});

	$(".pz .__PZ:last-child input[id$='_V']").live("keydown",function(e){
		readonly = $(this).attr("readonly");
		if((e.keyCode==9)&&$(this).val()!=""&&readonly==undefined){
			console.log("Tab")
			agregarPieza('piezas');
			console.log("Nueva pieza")
			return false;
		}		
	});

	$(".integer").live('keyup',function(){
		n = parseInt($(this).val());

		$(this).val(n);
		if(!(typeof n === 'number' && n % 1 == 0)){
			$(this).val('');
		}

		id = $(this).attr("id").substr(0,6);
		calcularNeto(__TOTALNETO);
	});

	$(".integer").live('focus',function(){
		if($(this).val()==''){
			$(this).val('0');
		}
		console.log("Focus")
		unirDuplicados();
		calcularNeto(__TOTALNETO);
	});

	$(".integer").live('blur',function(){
		if($(this).val()==''){
			$(this).val('0');
		}
		console.log("Blur")
		unirDuplicados();
		calcularNeto(__TOTALNETO);
	});

	$("[data-idpzu]").live('click',function(){
		var idpzu = $(this).attr("data-idpzu");
		var obj = $($($(this).parent()).parent());
		if(confirm('¿Realmente desea eliminar esta pieza?')){
			
			if(idpzu!='void'){
				$.ajax({url:base_url+"/ods/eliminarpzu/"+idpzu,
						type:"GET",
						success:function(){
							$(obj).remove();
							calcularNeto(__TOTALNETO);
						}
					});
			}
			else{
				$(obj).remove();
				calcularNeto(__TOTALNETO);
			}
		}
		return false;
	});

});