-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-04-2013 a las 10:11:21
-- Versión del servidor: 5.5.28
-- Versión de PHP: 5.4.6-1ubuntu1.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `clinicadigital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ClaseProducto`
--

CREATE DATABASE csa;
use csa;


CREATE TABLE IF NOT EXISTS `ClaseProducto` (
  `idClaseProducto` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idClaseProducto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cliente`
--

CREATE TABLE IF NOT EXISTS `Cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `identificacion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ciudad` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` text COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `celular` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hash` varchar(32) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Factura`
--

CREATE TABLE IF NOT EXISTS `Factura` (
  `idFactura` int(11) NOT NULL AUTO_INCREMENT,
  `idOrden` int(11) NOT NULL,
  `totaltrabajo` decimal(19,4) NOT NULL,
  `totalpiezas` decimal(19,4) NOT NULL,
  `descuento` decimal(19,4) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`idFactura`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Historial`
--

CREATE TABLE IF NOT EXISTS `Historial` (
  `idHistorial` int(11) NOT NULL AUTO_INCREMENT,
  `idOrden` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `mensaje` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`idHistorial`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Imagen`
--

CREATE TABLE IF NOT EXISTS `Imagen` (
  `idImagen` int(11) NOT NULL AUTO_INCREMENT,
  `idOrden` int(11) NOT NULL,
  `archivo` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idImagen`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MarcaProducto`
--

CREATE TABLE IF NOT EXISTS `MarcaProducto` (
  `idMarcaProducto` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idMarcaProducto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Orden`
--

CREATE TABLE IF NOT EXISTS `Orden` (
  `idOrden` int(11) NOT NULL AUTO_INCREMENT,
  `fechaentrada` date NOT NULL,
  `fechasalida` date DEFAULT NULL,
  `tipo` enum('P','G') COLLATE utf8_spanish_ci NOT NULL COMMENT 'P=PARTICULAR,\nG=GARANTIA',
  `abono` decimal(19,4) DEFAULT NULL,
  `observacion` text COLLATE utf8_spanish_ci,
  `accesorios` text COLLATE utf8_spanish_ci NOT NULL,
  `estadofisico` text COLLATE utf8_spanish_ci NOT NULL,
  `fechacompra` date DEFAULT NULL,
  `idTecnico` int(11) DEFAULT NULL,
  `falla` text COLLATE utf8_spanish_ci NOT NULL,
  `pendienterepuesto` text COLLATE utf8_spanish_ci,
  `idCliente` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `diagnostico` text COLLATE utf8_spanish_ci,
  `solucion` text COLLATE utf8_spanish_ci,
  `estado` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=PXD,2=DIAG.3=AUT,4=NAUT,5=DEV,6=REP,7=PXR,8=ENT,9=REIN,0=ANULADA',
  `creador` int(11) NOT NULL,
  `finalizador` int(11) DEFAULT NULL,
  PRIMARY KEY (`idOrden`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Pieza`
--

CREATE TABLE IF NOT EXISTS `Pieza` (
  `idPieza` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `valor` decimal(19,4) NOT NULL,
  PRIMARY KEY (`idPieza`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PiezaUsada`
--

CREATE TABLE IF NOT EXISTS `PiezaUsada` (
  `idPiezaUsada` int(11) NOT NULL AUTO_INCREMENT,
  `idOrden` int(11) NOT NULL,
  `idPieza` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valor` decimal(19,4) NOT NULL,
  PRIMARY KEY (`idPiezaUsada`),
  KEY `fk_PiezaUsada_1` (`idPieza`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Producto`
--

CREATE TABLE IF NOT EXISTS `Producto` (
  `idProducto` int(11) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idMarcaProducto` int(11) NOT NULL,
  `serie` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idClaseProducto` int(11) NOT NULL,
  PRIMARY KEY (`idProducto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tecnico`
--

CREATE TABLE IF NOT EXISTS `Tecnico` (
  `idTecnico` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idTecnico`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--

CREATE TABLE IF NOT EXISTS `Usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `username` varchar(16) COLLATE utf8_spanish_ci NOT NULL,
  `passwd` varchar(32) COLLATE utf8_spanish_ci NOT NULL,
  `nivel` enum('1','2','3') COLLATE utf8_spanish_ci NOT NULL COMMENT '1=ADMIN, 2=DIGITER, 3=READ_ONLY',
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
