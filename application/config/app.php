<?php 

	//Configuración de aplicación
	$config["app_name"] 		= "Centro de Servicio S.A";
	$config["app_eslogan"] 		= "Tecnología y Servicio";
	$config["app_nit"]		= "129673825-1";
	$config["app_version"]		= "1.0.8";
	$config["app_admin_email"]	= "admin@appmain.com";
	$config["app_telefono"] 	= "(1) 2452080";
	$config["app_direccion"]	= "Cll 1 # 2 - 3";

	//Configuraciones de apariencia
	$config["ui_style_background"] = "#004650";

	//Variables de sistema
	$config["sys_default_user_password"] 	= "empresa123";
	

 ?>
