<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$CI =& get_instance();

$html = '
<div class="well black">
    <h1>Ups! algo ha salido mal. :o</h1>
    <p>'.$message.'</p>
    <a href="'.base_url().'" class="btn btn-danger">Sacame de aquí!</a>
</div>';

$d["title"] = "Error";
$d["content"] = $html;
$CI->load->view('templates/template',$d);
