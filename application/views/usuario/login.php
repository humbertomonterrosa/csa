<div class="row-fluid">
	<div class="span6">
		<form method="post" class="form-signin">
		<h2 class="form-signin-heading relieve">Acceso de usuarios</h2>
		<p>Ingrese su nombre de usuario y contraseña para acceder a la plataforma, si aun no cuenta con uno contacte a el administrador del sistema.</p>
		<?php echo isset($err)?'<p class="text-error bold">'.$err.'</p>':'';?>
		<input type="text" name="user" class="input-block-level" placeholder="Nombre de usuario" autofocus="autofocus">
		<input type="password" name="passwd" class="input-block-level" placeholder="Contraseña">
		<br /><br />
		<input name="send" class="btn btn-large btn-primary" type="submit" value="Iniciar sesión" />
		</form>	
	</div>
</div>