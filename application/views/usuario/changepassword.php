<h1>Cambiar contraseña</h1>
<?php if(isset($err)){ echo '<div class="sep10 bold text-error">'.$err.'</div>';} ?>
<form method="post">
	<div class="row-fluid sep10">
		<div class="bold span2">Contraseña actual:</div>
		<div class="span10"><input type="password" name="currentpasswd" class="input-xlarge"></div>
	</div>
	<div class="row-fluid sep10">
		<div class="bold span2">Contraseña nueva:</div>
		<div class="span10"><input type="password" name="passwd" class="input-xlarge"></div>
	</div>
	<div class="row-fluid sep10">
		<div class="bold span2">Repita la contraseña nueva:</div>
		<div class="span4"><input type="password" name="repasswd" class="input-xlarge"></div>
	</div>
	<div class="sep10">
		<input type="submit" name="send" value="Cambiar contraseña" class="btn btn-success" />
	</div>

</form>