<h2>Restaurar base de datos</h2>
<?php echo isset($err)?'<div class="bold text-error sep10">'.$err.'</div>':''; ?>
<?php echo isset($suc)?'<div class="bold text-success sep10">'.$suc.'</div>':''; ?>
<div class="title_form sep10">Proceso de restauración</div>
<?php if(!isset($restaurando)){ ?>
<div class="sep10">Si ya tiene su copia de seguridad has clic en iniciar proceso de restauración, de lo contrario, descarguela y repita el mismo proceso.</div>
<div class="sep10">
	<form method="post">
		<input type="submit" class="btn btn-success" name="iniciar" value="Iniciar proceso de restauración" />
	</form>
</div>
<?php }else{ ?>
<div class="sep10">Seleccione su copia de seguridad y precione en el boton restaurar base de datos.</div>
<div class="sep10">
	<form method="post" enctype="multipart/form-data">
		<input type="file" name="archivo" /><br /><br />
		<input type="submit" class="btn btn-success" name="send" value="Restaurar base de datos" />
		<input type="submit" class="btn" name="cancelar" value="Cancelar" />
	</form>
</div>

<?php } ?>