<h2><?php echo $titulo; ?></h2>
<?php if(isset($err)){ echo '<div class="bold text-error">'.$err.'</div>';} ?>
<form method="post" class="confirm-form" data-msg="¿Desea guardar las modificaciones realizadas?.">
<div class="title_form sep10">
	Información de usuario
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Nombre: </div>
		<input type="text" name="nombre" class="fill_parent" value="<?php echo isset($nombre)?$nombre:''; ?>"/>
		<div class="text-error mar-5"><?php echo form_error('nombre');?></div>
	</div>
	<div class="span3">
		<div class="bold">Apellido: </div>
		<input type="text" name="apellido" class="fill_parent" value="<?php echo isset($apellido)?$apellido:''; ?>" />
		<div class="text-error mar-5"><?php echo form_error('apellido');?></div>
	</div>
	<div class="span6">
	</div>
</div>
<div class="row-fluid sep10">
	<?php if(isset($username)){ ?>
	<div class="span3">
		<div class="bold">Nombre de usuario: </div>
		<input type="text" name="username" class="fill_parent" value="<?php echo isset($username)?$username:''; ?>" />
		<div class="text-error mar-5"><?php echo form_error('username');?></div>
	</div>
	<?php } ?>
	<div class="span3">
		<div class="bold">Tipo de usuario: </div>
		<button type="button" data-index="<?php echo isset($nivel)&&$nivel!==false?$nivel-1:1; ?>" class="btn_multival btn btn-primary fill_parent" data-values="Administrador:1|Normal:2|Readonly:3" data-target="#nivel" ></button>
		<input id="nivel" type="hidden" name="nivel" />
	</div>
	<div class="span6"></div>
</div>
<?php if(isset($passwd)&&isset($repasswd)) {?>
<div class="row-fluid sep10">
	<div class="span3">
		<div class="bold">Contraseña: </div>
		<input type="password" name="passwd" class="fill_parent" />
		<div class="text-error mar-5"><?php echo form_error('passwd');?></div>
	</div>
	<div class="span3">
		<div class="bold">Verifique contraseña: </div>
		<input type="password" name="repasswd" class="fill_parent" />
		<div class="text-error mar-5"><?php echo form_error('repasswd');?></div>
	</div>
	<div class="span6">
	</div>
</div>
<?php } ?>
<div class="sep10">
	<input type="submit" class="btn btn-success" name="send" value="Guardar" /> <a href="<?php echo $link_cancel; ?>" class="btn">Cancelar</a>
</div>
</form>