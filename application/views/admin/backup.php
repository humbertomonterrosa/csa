<h2>Backup</h2>
<div class="title_form sep10">Copia de seguridad</div>
<div>Realice una copia de seguridad de la base de datos periodicamente para mantener su información a salvo, descargue la copia de seguridad y almacenelo en un host diferente al servidor.</div>
<div class="sep10"><a href="<?php echo base_url('admin/download_backup'); ?>" class="btn btn-success"><span class="icon-download icon-white"></span> Descargar copia de seguridad</a></div>

<div class="title_form sep10">Restaurar base de datos</div>
<div>Si ya cuenta con una copia de seguridad has clic en restaurar para iniciar el proceso de restauración de la base de datos.</div>
<div class="sep10"><a href="<?php echo base_url('admin/restaurar'); ?>" class="btn btn-success"><span class="icon-repeat icon-white"></span> Restaurar</a></div>