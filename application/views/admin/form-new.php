<h2><?php echo $titulo; ?></h2>
<?php if(isset($err)){ echo '<div class="sep10 bold text-error">'.$err.'</div>';} ?>
<form method="post">
	<div class="bold sep10">
		Descripción:
	</div>
	<input type="text" name="descripcion" value="<?php echo $descripcion; ?>">
	<div class="text-error mar-5"><?php echo form_error('descripcion');?></div>
	<div class="sep10">
		<input type="submit" class="btn btn-success" name="send" value="Guardar" /> <a href="<?php echo base_url($link_cancel); ?>" class="btn">Cancelar</a>
	</div>
</form>