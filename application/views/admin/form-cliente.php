<h2><?php echo $titulo; ?></h2>
<?php if(isset($err)){ echo '<div class="bold text-error">'.$err.'</div>';} ?>
<form method="post" class="confirm-form" data-msg="¿Desea guardar las modificaciones realizadas?.">
<div class="title_form sep10">
	Información de la empresa o cliente
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Nit/Cedula (*):</div>
		<input type="text" id="ident" name="ident" class="fill_parent" value="<?php echo $ident; ?>">
		<div class="text-error mar-5"><?php echo form_error('ident');?></div>
	</div>
	<div class="span5">
		<div class="bold">Empresa/Cliente (*):</div>
		<input id="nombre" type="text" name="nombre" class="fill_parent" value="<?php echo $nombre; ?>">
		<div class="text-error mar-5"><?php echo form_error('nombre');?></div>
	</div>
	<div class="span4">
		
	</div>
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Ciudad (*):</div>
		<input type="text" name="ciudad" class="fill_parent" value="<?php echo $ciudad; ?>">
		<div class="text-error mar-5"><?php echo form_error('ciudad');?></div>
	</div>
	<div class="span5">
		<div class="bold">Dirección (*):</div>
		<input type="text" name="direccion" class="fill_parent" value="<?php echo $direccion; ?>">
		<div class="text-error mar-5"><?php echo form_error('direccion');?></div>
	</div>
	<div class="span2">
		<div class="bold">Teléfono (*):</div>
		<input type="text" name="telefono" class="fill_parent" value="<?php echo $telefono; ?>">
		<div class="text-error mar-5"><?php echo form_error('telefono');?></div>
	</div>
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Celular:</div>
		<input type="text" name="celular" class="fill_parent" value="<?php echo $celular; ?>">
	</div>
</div>
<div class="sep10">
	<input type="submit" class="btn btn-success" name="send" value="Guardar" /> <a href="<?php echo $link_cancel; ?>" class="btn">Cancelar</a>
</div>
</form>