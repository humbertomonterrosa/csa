<h2><?php echo $titulo ?></h2>

<?php if((isset($add_link)&&isset($add_text))) { ?><a href="<?php echo base_url($add_link); ?>" class="btn btn-primary"><li class="icon-plus icon-white"></li> <?php echo $add_text; ?></a><?php } ?>
<div class="sep10"></div>
<table class="display dataTable" id="lista-tabla">
	<thead>
		<tr>
			<?php foreach ($tablecol as $key => $value) {
				echo '<td class="bold"'.(isset($value['width'])?' width="'.$value['width'].'"':'').'>'.$value['key_title'].'</td>';
			} ?>
		</tr>
	</thead>
	<tbody>
	<?php 
	foreach ($lista as $key => $value) {
	 	echo '<tr>';
	 	foreach ($tablecol as $k => $v) {
	 		echo '<td>'.$value[$v['key']].'</td>';
	 	}
	 	echo '</tr>';
	 } ?>
	</tbody>
</table>
<div class="clear-fix"></div>
<div class="sep10"></div>