<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title><?php echo 'REMISIÓN ODS'.$idOrden;?></title>
  <style type="text/css">
  body{
    margin: 0px;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  }

  .container{
    width: 850px;
    height: 512px;
    position: relative;
  }

  .header{
    margin-top: 0px;
    width: 850px;
    text-align: center;
    position: relative;
  }

  .header .logo{
    position: absolute;
    top: 5px;
    left: 70px;
  }

  .header .title{
    font-family: Georgia;
    font-size: 34px;
    font-weight: bold;
    font-style: italic;
    letter-spacing: -1px;
  }

  .header .subtitle{
    font-size: 16px;
    font-weight: bold;
    text-transform: uppercase;
    letter-spacing: -1px;
  }

  .header .subtitle2{
    font-size: 16px;
    text-transform: capitalize;
  }

  .header .subtitle3{
    font-size: 12px;
    text-transform: uppercase;
  }

  .right-content{
    top: 0px;
    right: 0px;
    position: absolute;
    width: 170px;
  }

  .numero-orden{
    padding: 7px;
    border:2px #000 solid;
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    border-radius: 4px;
    margin-bottom: 2px;
  }

  .tipo-servicio{
    font-size: 24px;
    text-align: center;
    letter-spacing: 2px;
    text-transform: capitalize;
    font-style: italic;
  }

  .content{
    margin-top:10px;
  }

  .fieldset:first-child{
    margin-right: 10px;
  }
  .fieldset{
    box-sizing:border-box;
    -webkit-box-sizing:border-box;
    -moz-box-sizing:border-box;
    padding-top: 16px;
    padding-bottom: 4px;
    position: relative;
    vertical-align: top;
    border-radius: 4px;
    width: 100%;
    margin: 0px;
    border: 2px #000 solid;
  }

  .fill_parent{
    width: 100%;
  }

  .fieldset .legend{
    border: 1px #000 solid;
    left: 10px;
    top: -10px;
    position: absolute;
    font-size: 12px;
    font-weight: bold;
    text-transform: capitalize;
    background-color: #fff;
    padding: 5px 10px;
    color: #111;
    border-radius: 4px;
  }

  .observaciones{
    margin-top: 10px;
    border: 2px #000 solid;
    border-radius: 4px;
    font-size: 12px;
  }

  .text_content{
    padding: 5px;
  }

  td{
    font-size: 12px;
  }

  .sep10{
    margin-top: 10px;
  }

  .descripcion{
    border: 2px #000 solid;
    border-radius: 4px;
    margin-top: 5px;
    font-size: 10px;
    padding: 2px;
  }
  
  .firma{   
    display: inline-block;
    position: relative;
    height: 100px;

  }

  .mifirma{
    border-top: 2px #000 solid;
    width: 70%;
    position: absolute;
    left: 0px;
    bottom: 0px;
    padding-top: 5px;
    text-align: center;
    font-weight: bold;
  }

  .foot{
    padding-top: 10px;
    font-size: 11px;
    text-align: center;
  }

  .low-title{
    font-size: 14px !important;
    font-weight: bold;
    text-align: center;
  }

  .fechas{
    font-size: 12px;
  }

  .bold{
    font-weight: bold;
  }

  .repadding{
    padding: 4px 0px;
  }

  .solucion{
    padding: 5px 15px;
    font-size: 16px;
    min-height: 70px;
  }
  .valortrabajo{
    padding: 4px;
    padding-right: 10px;
    text-align: right;
    border-bottom: 1px #000 solid;
  }

  .radius{
    border: 2px #000 solid;
    padding: 4px;
    border-radius: 4px;
  }

  .values td{
    font-size: 16px;
  }
  </style>
</head>
<body>
  <div class="container">
    <div class="header">
      <div class="logo"><img src="<?php echo base_url('img/logocd.png');?>"></div>
      <div class="title"><?php echo $this->config->item('app_name'); ?></div>
      <div class="subtitle">Centro de servicio autorizado</div>
      <div class="subtitle2"><?php echo $this->config->item('app_eslogan'); ?></div>
      <div class="subtitle3"><?php echo $this->config->item('app_direccion'); ?></div>
      <div class="subtitle3">Nit: <?php echo $this->config->item('app_nit'); ?></div>
    </div>
    <div class="right-content">
      <div class="low-title">REMISIÓN:</div>
      <div class="numero-orden"><?php echo 'ODS'.$idOrden;?></div>
      <div align="center" class="fechas">
        <b>Fecha Entrada:</b> <?php echo date("d/m/Y",strtotime($fechaentrada)); ?><br />
        <b>Fecha Salida:</b> <?php echo $fechasalida==""?'--/--/----':date("d/m/Y",strtotime($fechasalida)); ?>
      </div>
    </div>
    <div class="content">
      <br />
      <div class="fieldset">
        <div class="legend">Información del Servicio</div>
        <table width="100%">
          <tr>
            <td width="10%" align="right" class="bold">Nombre:</td>
            <td width="20%"><?php echo strtoupper($nombre);?></td>
            <td width="10%" align="right" class="bold">Identificación:</td>
            <td width="20%"><?php echo number_format($identificacion,0,".",".");?></td>
            <td width="10%" align="right" class="bold">Teléfono:</div>
            <td width="20%"><?php echo $telefono;?></div>
          <tr>
          <tr>
            <td width="10%" align="right" class="bold">Producto:</td>
            <td width="20%"><?php echo $clase;?></td>
            <td width="10%" align="right" class="bold">Marca:</td>
            <td width="20%"><?php echo $marca;?></td>
            <td width="10%" align="right" class="bold">Celular:</div>
            <td width="20%"><?php echo $celular;?></div>
          <tr>
          <tr>
            <td width="10%" align="right" class="bold">Modelo:</td>
            <td width="20%"><?php echo $modelo;?></td>
            <td width="10%" align="right" class="bold">Serie:</td>
            <td width="20%"><?php echo $serie;?></td>
            <td width="10%" align="right" class="bold"></div>
            <td width="20%"></div>
          <tr>
        </table>
      </div>
      <div class="fieldset bold sep10 repadding" align="center">Detalle de la remisión</div>
      <div class="solucion sep10">
        <div class="bold">Solución aplicada:</div>
        <?php echo $solucion;?>
      </div>
      <div class="valortrabajo">
        <?php 
        foreach ($pzu as $key => $value) {
           $totaltrabajo+=($value->cantidad*$value->valor);
         } ?>
        <b>Trabajo realizado:</b> <?php echo ' $ '.number_format($totaltrabajo,2,".",",");?>
      </div>
      <div class="firma fill_parent">
        <div class="right-content" style="top:10px; width:250px; text-align:right">
            <div class="radius">
              <table width="100%" class="values">
                <tr>
                  <td width="50%" class="bold" align="right">Subtotal:</td>
                  <td width="50%" align="right"><?php echo ' $ '.number_format($totaltrabajo,2,".",",");?></td>
                </tr>
                <tr>
                  <td width="50%" class="bold" align="right">Abono:</td>
                  <td width="50%" align="right"><?php echo ' ($ '.number_format($descuento,2,".",",").')';?></td>
                </tr>
                <tr>
                  <td width="50%" class="bold" align="right">Total:</td>
                  <td width="50%" align="right"><?php echo ' $ '.number_format($totaltrabajo-$descuento,2,".",",");?></td>
                </tr>
              </table>
              </div>
        </div>
        <div class="mifirma" style="width:350px; margin-top:70px;">Firma de Cliente</div>
      </div>
      <div class="foot sep10">
        Clinica Digital <?php echo date("Y");?>. <b>Dir:</b> <?php echo $this->config->item('app_direccion'); ?> <b>Tel:</b> <?php echo $this->config->item('app_telefono'); ?> <b>E-mail:</b> <?php echo $this->config->item('app_admin_email'); ?>
      </div>
  </div>
  <script type="text/javascript">
    window.print();
  </script>
</body>
</html>