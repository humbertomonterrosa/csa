<div align="right" class="btn-group">
	<a href="<?php echo base_url('ods/ver/ODS'.$idOrden);?>" class="btn btn-info"><span class="icon-arrow-left icon-white"></span> Volver a ODS</a>
</div>

<h2>Imagenes de la orden ODS<?php echo $idOrden; ?></h2>

<?php if(count($lista)==0) echo '<div class="sep10">No hay imagenes para esta orden</div>'; ?>
<?php foreach ($lista as $key => $value) { ?>
<div class="img-producto img-polaroid">
	<a href="<?php echo base_url('uploads/'.$value->archivo); ?>" class="imagen" rel="prettyPhoto" alt="Descripcion" title="Titulo" data-img="<?php echo base_url('uploads/'.$value->archivo); ?>">
		<img src="<?php echo base_url('uploads/'.$value->archivo); ?>" width="0" />
	</a>
	<?php if($creador==uid()||isuser('ADMIN')){ ?>
	<a href="<?php echo base_url('ods/eliminarimagen/'.$value->idImagen.'/'.$value->archivo); ?>" class="delete btn btn-danger btn-mini link-confirm" data-msg="¿Desea eliminar esta imagen?"><li class="icon-trash icon-white"></li> Eliminar</a>
	<?php } ?>
</div>
<?php } ?>

<div class="title_form sep10">Subir imagen</div>
<form method="post" enctype="multipart/form-data">
	<strong>Seleccione imagen: </strong> <input type="file" name="imagen" />
	<div class="sep10">
		<input type="submit" class="btn btn-success" name="send" value="Guardar imágen" />
	</div>
</form>