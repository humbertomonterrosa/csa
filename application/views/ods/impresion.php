<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title><?php echo 'ODS'.$idOrden;?></title>
  <style type="text/css">
  body{
    margin: 0px;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  }

  .container{
    width: 850px;
    height: 512px;
    position: relative;
  }

  .header{
    margin-top: 0px;
    width: 850px;
    text-align: center;
    position: relative;
  }

  .header .logo{
    position: absolute;
    top: 5px;
    left: 80px;
  }

  .header .logo img{
    height: 85px;
  }

  .header .title{
    font-family: Georgia;
    font-size: 34px;
    font-weight: bold;
    font-style: italic;
    letter-spacing: -1px;
  }

  .header .subtitle{
    font-size: 16px;
    font-weight: bold;
    text-transform: uppercase;
    letter-spacing: -1px;
  }

  .header .subtitle2{
    font-size: 16px;
    text-transform: capitalize;
  }

  .header .subtitle3{
    font-size: 12px;
    text-transform: uppercase;
  }

  .right-content{
    top: 0px;
    right: 0px;
    position: absolute;
    width: 170px;
  }

  .numero-orden{
    padding: 4px 7px;
    border:2px #000 solid;
    text-align: center;
    font-size: 25px;
    font-weight: bold;
    border-radius: 4px;
  }

  .tipo-servicio{
    font-size: 18px;
    text-align: center;
    letter-spacing: 2px;
    text-transform: capitalize;
    font-style: italic;
  }

  .content{
    margin-top:10px;
  }

  .fieldset:first-child{
    margin-right: 10px;
  }
  .fieldset{
    box-sizing:border-box;
    -webkit-box-sizing:border-box;
    -moz-box-sizing:border-box;
    padding-top: 16px;
    padding-bottom: 4px;
    position: relative;
    vertical-align: top;
    border-radius: 4px;
    width: 414px;
    margin: 0px;
    display: inline-block;
    border: 2px #000 solid;
    font-size: 11px;
  }

  .fill_parent{
    width: 100%;
  }

  .fieldset .legend{
    border: 1px #000 solid;
    left: 10px;
    top: -10px;
    position: absolute;
    font-size: 12px;
    font-weight: bold;
    text-transform: capitalize;
    background-color: #fff;
    padding: 5px 10px;
    color: #111;
    border-radius: 4px;
  }

  .observaciones{
    margin-top: 10px;
    border: 2px #000 solid;
    border-radius: 4px;
    font-size: 12px;
  }

  .text_content{
    padding: 5px;
  }

  td{
    font-size: 11px;
    padding: 0px;
  }

  .sep10{
    margin-top: 10px;
  }

  .descripcion{
    border: 2px #000 solid;
    border-radius: 4px;
    margin-top: 5px;
    font-size: 10px;
    padding: 2px;
  }
  
  .firma{   
    vertical-align: top;
    width: 418px;
    display: inline-block;
  }

  .mifirma{
    border-top: 2px #000 solid;
    width: 70%;
    margin-top: 40px;
    padding-top: 5px;
    text-align: center;
    font-weight: bold;
  }

  .foot{
    padding-top: 0px;
    font-size: 11px;
    text-align: center;
  }
  </style>
</head>
<body>
  <div class="container">
    <div class="header">
      <div class="logo"><img src="<?php echo base_url('img/logocd.png');?>"></div>
      <div class="title"><?php echo $this->config->item('app_name'); ?></div>
      <div class="subtitle">Centro de servicio autorizado</div>
      <div class="subtitle2"><?php echo $this->config->item('app_eslogan'); ?></div>
      <div class="subtitle3"><?php echo $this->config->item('app_direccion'); ?></div>
      <div class="subtitle3">Nit: <?php echo $this->config->item('app_nit'); ?></div>
    </div>
    <div class="right-content">
      <div class="numero-orden"><?php echo 'ODS'.$idOrden;?></div>
      <div align="center"><b>Fecha Entrada</b><br /><?php echo date("d/m/Y",strtotime($fechaentrada)); ?></div>
      <div class="tipo-servicio"><?php echo ($tipo=='G'?'Garantia':($tipo=='P'?'Particular':''))?></div>
    </div>
    <div class="content">
      <div class="fieldset">
        <div class="legend">Información del Cliente</div>
        <table width="100%">
          <tr>
            <td width="30%" align="right"><b>Nit/Cedula:</b></td>
            <td colspan="3"><?php echo is_integer($identificacion)?number_format($identificacion,0,'.','.'):$identificacion;?></td>
          <tr>
          <tr>
            <td width="33,33%" align="right"><b>Empresa/Nombre:</b></td>
            <td colspan="3"><?php echo ucwords($nombre); ?></td>
          <tr>
          <tr>
            <td width="33,33%" align="right"><b>Ciudad:</b></td>
            <td colspan="3"><?php echo ucwords($ciudad);?></td>
          <tr>
          <tr>
            <td width="33,33%" align="right"><b>Dirección:</b></td>
            <td colspan="3"><?php echo strtoupper($direccion);?></td>
          <tr>
          <tr>
            <td width="33,33%" align="right"><b>Teléfono:</b></td>
            <td width="16,66%"><?php echo $telefono;?></td>
            <td width="16,66%" align="right"><b>Celular:</b></td>
            <td width="33,33%"><?php echo $celular;?></td>
          <tr>
        </table>
      </div>
      <div class="fieldset">
        <div class="legend">Información del Producto</div>
        <table width="100%">
          <tr>
            <td width="20%" align="right"><b>Marca:</b></td>
            <td width="16,66%"><?php echo strtoupper($marca);?></td>
            <td width="16,66%" align="right"><b>Producto:</b></td>
            <td width="33,33%"><?php echo strtoupper($clase);?></td>
          <tr>
          <tr>
            <td width="33,33%" align="right"><b>Modelo:</b></td>
            <td width="16,66%"><?php echo $modelo;?></td>
            <td width="16,66%" align="right"><b>Serie:</b></td>
            <td width="33,33%"><?php echo $serie;?></td>
          <tr>
          <tr>
            <td width="33,33%" align="right"><b>Falla:</b></td>
            <td colspan="3"><?php echo $falla;?></td>
          <tr>
          <tr>
            <td width="33,33%" align="right"><b>Estado:</b></td>
            <td colspan="3"><?php echo $estadofisico;?></td>
          <tr>
          <tr>
            <td width="33,33%" align="right"><b>Accesorios:</b></td>
            <td colspan="3"><?php echo $accesorios;?></td>
          <tr>
        </table>
      </div>
      <div class="fieldset observaciones fill_parent">
        <div class="legend">Observaciones</div>
        <div class="text_content"><?php echo $observacion;?></div>
      </div>
      <div class="sep10" align="right">
        <b>Abono:</b> $ <?php echo number_format($abono,2);?>
      </div>
      <div class="descripcion">
        <b>1°</b> El articulo aqui detallado tiene Garantia por 30 dias (Servicios Tecnicos)sobre la reparación  efectuada ; cualquier repuesto utilizado en este tiempo lo cubre el dueño del Articulo.
        <b>2°</b> Solamente con la presentación de este comprobante se hará la devolución del articulo.
        <b>3°</b> El pago de las reparaciones es estrictamente de contado.
        <b>4°</b> Pasado 30 días desde la fecha de reparación, si el cliente no ha retirado el articulo el taller cobrará $500 diarios de bodegaje mas el 4% de su reparacion total.
        <b>5°</b> No nos responsabilizamos cuando en un articulo electronico por cuestiones de reparaciones tecnicas se llegue a borrar una informacion o algo que contenga grabado.
      </div>
      <br />
      <div class="firma" style="margin-right:10px;">
        <div class="mifirma">Firma ASC</div>
      </div>
      <div class="firma">
        <div class="mifirma">Firma de Cliente</div>
      </div>
      <div class="foot sep10">
        <?php echo $this->config->item('app_name'); ?> <?php echo date("Y");?>. <b>Dir:</b> <?php echo $this->config->item('app_direccion'); ?> <b>Tel:</b> <?php echo $this->config->item('app_telefono'); ?> <b>E-mail:</b> <?php echo $this->config->item('app_admin_email'); ?>
      </div>
  </div>
  <script type="text/javascript">
    window.print();
    if(typeof(JSWindow)!="undefined"){
      JSWindow.print();
    }
  </script>
</body>
</html>