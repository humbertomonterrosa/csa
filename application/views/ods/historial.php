<div class="btn-group">
	<a href="<?php echo base_url('ods/ver/ODS'.$idOrden);?>" class="btn btn-info"><span class="icon-arrow-left icon-white"></span> Volver a ODS</a>
</div>
<h2>Historial de la ORDEN <?php echo 'ODS'.$idOrden; ?></h2>

<div class="sep10">
	<?php if(count($historial)==0){
		echo '<span class="text-error">No hay historial para esta orden.</span>';
	}
	else{
		foreach ($historial as $key => $value) {
			echo '<div class="sep10">['.date("d/m/Y h:i a",strtotime($value->fecha)).'] <strong>'.$value->nombre.'</strong> '.$value->mensaje.'</div>';
		}
	} ?>
</div>