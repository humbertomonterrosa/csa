<?php

if($estado>=3&&$estado<=8){
	?>
	<div class="title_form">
		ESTADO FINAL
	</div>
	<?php
	if($estado==4||$estado==5||$estado==6||$estado==8){
	?>
	<div class="bold">Estado final del producto:</div>
	<div><?php echo strtoupper(estadoOrden($estado,false));?></div>
	<div class="bold sep10">Solución aplicada:</div>
	<div><?php echo $solucion;?></div>	
	<?php
	}
	else{
		?>
		<div>
		<form action="<?php echo base_url('ods/estadofinal/ODS'.$idOrden); ?>" method="post" class="confirm-form" data-msg="He verificado la solucion aplicada que he ingresado y deseo darle fin a esta orden.">
			<div class="bold">Solución aplicada:</div>
			<textarea name="solucion" class="fill_parent"><?php echo isset($solucion)?$solucion:'';?></textarea>
			<div class="text-error mar-5"><?php echo form_error('solucion');?></div>
			<div class="sep5">
				<?php if($estado!=6){?><button class="btn btn-success" name="op1" value="ok">Reparado</button><?php } ?>
				<button class="btn btn-danger" name="op2" value="ok">No reparable</button>
			</div>
		</form>
		</div>
		<?php
	}
}

?>