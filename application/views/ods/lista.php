	<h3><?php echo isset($q)?'Busqueda':'Lista';?> de ordenes de servicio</h3>
	<?php
	if(isset($q)){
		echo '<p><b>Usted busca:</b> '.htmlentities($q).'</p>';
	}
	else{
		echo '<p>Lista de ordenes guardadas recientemente</p>';
	}
	?>
	<?php echo $paginas;?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr bgcolor="#FFF">
				<th width="">ODS</th>
				<th width="20" class="hidden-phone">SERVICIO</th>
				<th width="100">FECHA</th>
				<th width="220">CLIENTE</th>
				<th width="205">PRODUCTO</th>
				<th class="hidden-phone">FALLA</th>
				<th width="50">Estado</th>
			</tr>
		</thead>
		<tbody>
			<?php
			for($i=$page;$i<($page+$per_page)-1;$i++) {
				if(!isset($ordenes[$i])) break;
				$value = $ordenes[$i];
				echo '<tr>';
				echo '<td class="middle nowrap"><a href="'.base_url('ods/ver/ODS'.$value->idOrden).'" class="bold font12"><li class="icon-eye-open"></li> ODS'.$value->idOrden.'</a></td>';
				echo '<td class="hidden-phone middle" align="center"><span class="label'.($value->tipo=="G"?' label-important">Garantia':' label-inverse">Particular').'</span></td>';
				echo '<td class="top nowrap"><strong>Ent:</strong> '.date("d/m/Y",strtotime($value->fechaentrada)).'<br />';
				echo '<strong>Sal:</strong> '.($value->fechasalida==null?'--/--/----':date("d/m/Y",strtotime($value->fechasalida))).'</td>';
				echo '<td class="middle"><a class="show-hand" data-placement="right" data-content="<strong>Teléfono:</strong> '.$value->telefono.'<br /><strong>Celular:</strong> '.$value->celular.'<br /><strong>Dirección:</strong> '.$value->direccion.', '.$value->ciudad.'<br /> <a href=\''.base_url('ods/buscar/'.$value->identificacion).'\'>Mas ODS de este cliente</a>" data-original-title="Información del cliente" rel="popover">'.$value->nombre.'</a></td>';
				echo '<td class="font12 middle"><a class="icon-barcode" rel="tooltip" data-placement="left" title="<span class=\'font12 bold\'>Serial: '.$value->serie.'</span>"></a> '.$value->clase.' '.$value->marca.' '.$value->modelo.'</td>';
				echo '<td class="font12 top hidden-phone">'.$value->falla.'</td>';
				echo '<td class="font12 middle"><a href="'.base_url('ods/buscar/ODS STATUS '.$value->estado).'">'.estadoOrden($value->estado).'</a></td>';
				echo '</tr>';	
			}
			?>
		</tbody>
	</table>
	<?php echo $paginas;?>

	<p class="font11">
		<?php
			for($i=1;$i<=9;$i++){
				echo '<span class="label label-inverse">'.estadoOrden($i).': '.estadoOrden($i,false).'</span>&nbsp;&nbsp;&nbsp;';
			}
		?>

	</p>