<?php
if($estado==2){
?>
<div class="title_form">
	<a name="autorizacion">Proceso de reparación</a>
</div>
<div>
	<p>Seleccione el camino que desea darle a esta orden de servicio.</p>
	<form action="<?php echo base_url('ods/autorizacion/ODS'.$idOrden);?>" method="post">
		<button type="submit" name="op1" value="ok" class="btn btn-success confirm-seleccion" data-item="Autorizar reparación">Autorizar reparación</button>
		<button type="submit" name="op2" value="ok" class="btn btn-danger confirm-seleccion" data-item="No autorizar reparación">No autorizar reparación</button>
		<button type="submit" name="op3" value="ok" class="btn btn-danger confirm-seleccion" data-item="Devolución">Devolución</button>
	</form>
</div>
<?php
}
?>