<?php

if($estado==3||$estado==7){
	?>
	<div class="sep10">
		<div class="bold"><a name="pxr">Repuestos pendientes:</a></div>
		<form class="form-ajax" action="<?php echo base_url('ods/pxr/ODS'.$idOrden);?>" data-init="(function(){$('#savepxr').attr('disabled','disabled')})" data-finalize="(function(){$('#savepxr').removeAttr('disabled')})" data-success="(function(a){Notify.show(a.msg)})" data-error="(function(a,b,c){Notify.show('Error: '+a.status+' '+c)})" method="post">
			<input type="text" name="pxr" class="input-xxlarge" value="<?php echo $pendienterepuesto;?>"/>
			<div class="sep5"><input id="savepxr" type="submit" name="send" class="btn btn-success" value="Guardar repuestos pendientes"></div>
		</form>
	</div>
	<?php
}
else if(($estado==6||$estado==8)&&$pendienterepuesto!=null){
	?>
	<div class="sep10">
		<div class="bold"><a name="pxr">Repuestos pendientes:</a></div>
		<div><?php echo $pendienterepuesto;?></div>
	</div>
	<?php
}
?>