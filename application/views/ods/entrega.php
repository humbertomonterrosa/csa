<form method="post">
<div class="btn-group">
	<a href="<?php echo base_url('ods/ver/ODS'.$idOrden); ?>" class="btn btn-info"><span class="icon-arrow-left icon-white"></span> Volver a ODS</a>
	<button type="submit" class="btn btn-success confirm-seleccion" data-item="Entregar producto y cerrar ODS" name="entregar" value="OK"><li class="icon-ok icon-white"></li> Entregar producto y cerrar ODS</button>
</div>
</form>
<h2>Entrega de <?php echo $clase." ".$marca." ".$modelo." [ODS".$idOrden."][".($tipo=='G'?'GARANTIA':'PARTICULAR')."]"; ?></h2>
<div class="row-fluid">
	<div class="span6">
		<div class="sep10 title_form">Estado final del producto</div>
		<div class="bold">Estado del producto:</div>
		<div<?php echo ($estado==4||$estado==5)?' class="text-red bold"':' class="text-green bold"'; ?>><?php echo estadoOrden($estado,false); ?></div>
		<div class="sep10 bold">Diagnostico:</div>
		<div><?php echo $diagnostico; ?></div>
		<div class="sep10 bold">Solución aplicada:</div>
		<div><?php echo $solucion; ?></div>
		<div class="sep10 bold">Observaciones:</div>
		<div><?php echo $observacion; ?></div>
	</div>
	<div class="span6">
		<div class="sep10 title_form">Facturación</div>
		<?php
		if(count($factura)!=0){
			$totaltrabajo = $factura->totaltrabajo;
			$abono = $factura->descuento;
			$totalpiezas = $factura->totalpiezas;
		}
		else{
			$totaltrabajo = 0;
			$abono = $abono;
			$totalpiezas = 0;
		}
		$total = ($totaltrabajo-$abono)+$totalpiezas;
		?>
		<div class="row-fluid sep10 text14px">
			<div class="bold span6">Total trabajo</div>
			<div class="span6"><?php echo "$ ".number_format($totaltrabajo,2,',','.');?></div>
		</div>
		<div class="row-fluid sep10 text14px">
			<div class="bold span6">Total repuestos</div>
			<div class="span6"><?php echo "$ ".number_format($totalpiezas,2,',','.');?></div>
		</div>
		<div class="row-fluid sep10 text14px">
			<div class="bold span6">Abono</div>
			<div class="span6"><?php echo "($ ".number_format($abono,2,',','.').")";?></div>
		</div>
		<div class="row-fluid sep10 text14px">
			<div class="bold span6">TOTAL</div>
			<div class="span6"><?php echo "$ ".number_format($total,2,',','.');?></div>
		</div>
	</div>
</div>