<div align="right" class="btn-group">
	<a href="<?php echo base_url('ods'); ?>" class="btn btn-info"><span class="icon-arrow-left icon-white"></span> Volver</a>
	<a href="<?php echo base_url('ods/historial/ODS'.$idOrden); ?>" class="btn btn-info"><span class="icon-time icon-white"></span> Historial</a>
	<a href="<?php echo base_url('ods/imagenes/ODS'.$idOrden);?>" class="btn btn-info"><span class="icon-camera icon-white"></span> Imágenes</a>
	<?php if($estado==2||$estado==3||($estado>=5&&$estado<=8)){?><a href="<?php echo base_url('ods/facturar/ODS'.$idOrden.'/').(count($this->factura->getByOrden($idOrden))==0?'#facturar':'');?>" class="btn btn-info"><span class="icon-file icon-white"></span> <?php echo $estado==2?'Cotizar':'Facturar'; ?></a><?php } ?>
	<a href="<?php echo base_url('ods/odsprint/ODS'.$idOrden);?>" target="_blank" class="btn btn-info"><span class="icon-print icon-white"></span> Imprimir</a>
	<?php if($estado>=4&&$estado<=6){ ?><a href="<?php echo base_url('ods/entregar/ODS'.$idOrden); ?>" class="btn btn-success"><span class="icon-inbox icon-white"></span> Entregar producto</a><?php } ?>
	<?php if($estado==8){ ?><a href="<?php echo base_url('ods/reingreso/ODS'.$idOrden); ?>" class="btn btn-success"><span class="icon-repeat icon-white"></span> Reingreso</a><?php } ?>
	<?php if(isuser('ADMIN')){ ?><a href="<?php echo base_url('ods/anular/ODS'.$idOrden); ?>" class="btn btn-danger"><span class="icon-ban-circle icon-white"></span> Anular</a> <?php } ?>

</div>
<h2>ORDEN ODS<?php echo $idOrden;?> [<?php echo estadoOrden($estado,false); ?>]</h2>
<div class="row-fluid">
	<div class="pull-right">
		<div class="control-group">
			<div class="controls">
				<div class="input-prepend">
					<span class="add-on" style="width:200px"><b>Fecha Entrada:</b> <?php echo date("d-m-Y",strtotime($fechaentrada));?></span>
				</div>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<div class="input-prepend">
					<span class="add-on" style="width:200px"><b>Fecha Salida:</b> <?php echo $fechasalida==null?'--/--/---':date("d-m-Y",strtotime($fechasalida));?></span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="title_form">
	Información de la empresa o cliente <?php if(isuser('ADMIN')) {?><a href="<?php echo base_url('admin/cliente_editar/'.$idCliente); ?>" class="btn btn-primary btn-mini pull-right"><li class="icon-edit icon-white"></li> Editar cliente</a><?php } ?>
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Nit/Cedula de ciudadania:</div>
		<div><?php echo $identificacion;?></div>
	</div>
	<div class="span5">
		<div class="bold">Empresa/Cliente:</div>
		<div><?php echo $nombre;?></div>
	</div>
	<div class="span4">
		
	</div>
</div>
<div class="row-fluid sep10">
	<div class="span3">
		<div class="bold">Ciudad:</div>
		<div><?php echo $ciudad;?></div>
	</div>
	<div class="span3">
		<div class="bold">Dirección:</div>
		<div><?php echo $direccion;?></div>
	</div>
	<div class="span2">
		<div class="bold">No. Teléfono:</div>
		<div><?php echo $telefono;?></div>
	</div>
	<div class="span4">
		<div class="bold">No. Celular:</div>
		<div><?php echo $celular;?></div>
	</div>
</div>
<div class="title_form">
	Datos del producto <?php if(isuser('ADMIN')||(isuser('NORMAL')&&$creador==uid()&&$estado!=8)) {?><a href="<?php echo base_url('ods/editarproducto/ODS'.$idOrden); ?>" class="btn btn-primary btn-mini pull-right"><li class="icon-edit icon-white"></li> Editar producto</a><?php } ?>
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Marca Electrodomestico:</div>
		<div><?php echo $marca;?></div>
	</div>
	<div class="span3">
		<div class="bold">Clase Electrodomestico:</div>
		<div><?php echo $clase;?></div>
	</div>
	<div class="span2">
		<div class="bold">Modelo:</div>
		<div><?php echo $modelo;?></div>
	</div>
	<div class="span4">
		<div class="bold">No. Serie:</div>
		<div><?php echo $serie;?></div>
	</div>
</div>
<div class="row-fluid sep10">
	<div class="span3">
		<div class="bold">Falla:</div>
		<div><?php echo $falla;?></div>
	</div>
	<div class="span3">
		<div class="bold">Estado físico:</div>
		<div><?php echo $estadofisico;?></div>
	</div>
	<div class="span6">
		<div class="bold">Accesorios:</div>
		<div><?php echo $accesorios;?></div>
	</div>
</div>
<div class="title_form">
	Información de servicio <?php if(isuser('ADMIN')||(isuser('NORMAL')&&$creador==uid()&&$estado!=8)) {?><a href="<?php echo base_url('ods/editarservicio/ODS'.$idOrden); ?>" class="btn btn-primary btn-mini pull-right"><li class="icon-edit icon-white"></li> Editar servicio</a><?php } ?>
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Prestación de servicio:</div>
		<div><?php echo ($tipo=="P"?'PARTICULAR':($tipo=="G"?'GARANTIA':''));?></div>
	</div>
	<div class="span9">
		<div class="bold">Abono:</div>
		<div><?php echo "$ ".number_format($abono,0,',','.');?></div>
	</div>
</div>
<div class="bold sep10">Creador de ODS:</div>
<div><?php $user = $this->usuario->get($creador); echo strtoupper($user->nombre.' '.$user->apellido);?></div>
<?php if($finalizador!=null){ ?>
<div class="bold sep10">Finalizador de ODS:</div>
<div><?php $user = $this->usuario->get($finalizador); echo strtoupper($user->nombre.' '.$user->apellido);?></div>
<?php } ?>
<div class="row-fluid sep10">
	<div class="span12">
		<form class="form-ajax" data-init="(function(){$('#saveobs').attr('disabled','disabled')})" data-finalize="(function(){$('#saveobs').removeAttr('disabled')})" data-success="(function(a){Notify.show(a.msg)})" action="<?php echo base_url('ods/observacion/ODS'.$idOrden);?>" method="post">
		<div class="bold">Observaciones:</div>
		<?php if($estado==8) {?>
		<div><?php echo $observacion;?></div>
		<?php }else{ ?>
		<div><textarea name="observacion" class="fill_parent"><?php echo $observacion;?></textarea></div>
		<div id="notify"></div>
		<div><input id="saveobs" type="submit" class="btn btn-success" name="send" value="Guardar observación"></div>
		<?php } ?>
		</form>
	</div>
</div>
<?php
	$this->load->view('ods/diagnostico',$GLOBALS);
	$this->load->view('ods/autorizacion',$GLOBALS);
	$this->load->view('ods/pxr',$GLOBALS);
	$this->load->view('ods/estadofinal',$GLOBALS);
if(isset($saved)&&$saved){
?>
<div class="print-object"><iframe src="<?php echo base_url('ods/odsprint/ODS'.$idOrden);?>" width="1" height="1" style="visibility:hidden"></iframe></div>
<?php
}
?>