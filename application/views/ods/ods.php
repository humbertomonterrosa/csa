<h2 class="relieve"><?php echo isset($page_title)?strtoupper($page_title):''; ?></h2>
<div>
	<div class="navbar navbar-inverse">
  <div class="navbar-inner">
    <span class="brand visible-desktop">Buscar:</span>
    <form action="<?php echo base_url('ods/buscar');?>" method="post" class="navbar-search">
    	<input type="text" id="buscarods" name="q" class="search-query" value="<?php echo isset($q)?$q:'';?>" placeholder="Ingrese ODS o nombre de cliente"> <a class="icon-info-sign icon-white show-hand" rel="popover" data-placement="right" title="¿Como buscar?" data-content="Para buscar puede ingresar No. de ODS, identificación, nombre del cliente o el modelo de su electrodomestico.<br /><i>Ej: TV LED 32LM4300</i>"></a>
    </form>
    <ul class="nav pull-right">
	<li class="divider-vertical"></li>
    <li class="hidden-phone"><a href="<?php echo base_url('ods');?>"><span class="icon-list icon-white"></span> Lista ODS</a><li>
    <li class="visible-phone"><a href="<?php echo base_url('ods');?>"><span class="icon-list icon-white"></span></a><li>
	<li class="hidden-phone"><a href="<?php echo base_url('ods/nueva');?>"><span class="icon-plus-sign icon-white"></span> Nueva ODS</a><li>    	
    <li class="visible-phone"><a href="<?php echo base_url('ods/nueva');?>"><span class="icon-plus-sign icon-white"></span></a><li>     
    <?php if(isuser('ADMIN')){ ?>
    <li class="hidden-phone"><a href="<?php echo base_url('ods/anuladas');?>"><span class="icon-ban-circle icon-white"></span> ODS Anuladas</a><li>        
    <li class="visible-phone"><a href="<?php echo base_url('ods/anuladas');?>"><span class="icon-ban-circle icon-white"></span></a><li>     
    <?php } ?>   
    </ul>
  </div>
</div>
</div>

<div class="well black default-height">
<?php echo $content;?>
</div>