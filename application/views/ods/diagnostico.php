<div class="title_form">
	<a name="diagnostico">Diagnóstico Técnico</a>
</div>
<?php
if($estado==1||$estado==9){
?>
<form action="<?php echo base_url('ods/diagnosticar/ODS'.$idOrden);?>" method="post" class="confirm-form" data-msg="He verificado el diagnóstico ingresado y deseo guardarlo.">
	<div>
		<p>Orden de servicio pendiente por diagnóstico técnico.</p>
		<div>
			<div class="bold">Técnico encargado:</div>
			<input type="text" id="tecnico" name="tecnico" value="<?php echo isset($tecnico)?$tecnico:'';?>">
			<div class="text-error mar-5"><?php echo form_error('tecnico');?></div>
			<script type="text/javascript">
			$("#tecnico").typeahead({
									source:function(typeahead,query){
										return $.ajax({url:base_url+"suggest/tecnico/",type:"POST",dataType:"json",data:"nombre="+typeahead,success:function(data){return query(data);}});
									}
			});
		</script>
		</div>
		<div>
			<div class="bold">Diagnóstico del producto:</div>
			<textarea name="diagnostico" class="fill_parent"><?php echo isset($diagnostico)?$diagnostico:'';?></textarea>
			<div class="text-error mar-5"><?php echo form_error('diagnostico');?></div>
		</div>
		<div class="sep10">
			<input type="submit" name="send" class="btn btn-success" value="Diagnosticar producto">
		</div>
	</div>
</form>
<?php
}
else{
	?>
	<div>
		<div>
			<div class="bold">Técnico encargado:</div>
			<div><?php echo $nombretecnico;?></div>
			<div class="bold sep10">Diagnóstico del producto:</div>
			<div><?php echo $diagnostico;?></div>
		</div>
	</div>
	<?php
}
?>