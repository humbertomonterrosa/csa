<script type="text/javascript" src="<?php echo base_url('js/facturacion.js');?>"></script>
<div align="right" class="btn-group">
	<a href="<?php echo base_url('ods/ver/ODS'.$idOrden);?>" class="btn btn-info"><span class="icon-arrow-left icon-white"></span> Volver a ODS</a>
	<?php if($factura){ ?> <a href="<?php echo base_url('ods/remprint/ODS'.$idOrden);?>" target="_blank" class="btn btn-info"><span class="icon-print icon-white"></span> Imprimir remisión</a><?php } ?>
</div>
<h2><?php echo $estado==2?'COTIZACIÓN':'FACTURACIÓN'; ?> ODS<?php echo $idOrden;?></h2>
<div class="title_form">
</div>
<div class="text-error mar-5"><?php echo isset($err)?$err:'';?></div>
<form method="post">
<div class="row-fluid sep10">
	<div class="span6">
		<div class="row-fluid __HEAD">
			<div class="span7 bold" align="center">Pieza</div>
			<div class="span2 bold" align="center">Cantidad</div>
			<div class="span3 bold" align="center">Precio Unidad</div>
		</div>		
		<div id="piezas" class="pz">
		</div>
		<script type="text/javascript">
		<?php
		if($npiezas!=0){
			for($i=0;$i<$npiezas;$i++){
				if($pieza[$i]!=null){
				echo 'agregarPieza("piezas","'.$pieza[$i].'","'.$cantidad[$i].'","'.$valor[$i].'",'.$idpzu[$i].',false'.($estado==8?',"all"':'').');';
				}
			}
		}
		?>
		</script>
		<?php if($estado!=8){ ?>
		<div class="text-right sep10"><a href="#" class="btn btn-primary btn-mini add-pz" data-content-pz="piezas"><li class="icon-plus-sign icon-white"></li> Agregar pieza</a></div>
		<?php } ?>
	</div>
	<div class="span4">
		<div class="well white-background">
			<div class="title_form">Valores</div>
			<div class="row-fluid">
				<div class="span6 bold">Total piezas:</div>
				<div class="span6"><input id="totalpiezas" type="text" readonly="readonly" class="input-small text-right"></div>
			</div>
			<div class="row-fluid">
				<div class="span6 bold">Total trabajo:</div>
				<div class="span6"><input id="totaltrabajo"<?php if($estado==8){ echo ' readonly="readonly"'; } ?> name="trabajo" type="text" class="input-small text-right integer" value="<?php echo isset($trabajo)?$trabajo:'0';?>"></div>
			</div>
			<div class="row-fluid">
				<div class="span6 bold">Abono:</div>
				<div class="span6"><input id="descuento"<?php if($estado==8){ echo ' readonly="readonly"'; } ?> name="descuento" type="text" class="input-small text-right integer" value="<?php echo $descuento;?>"></div>
			</div>
			<div class="title_form">Total Neto</div>
			<div id="totalneto" class="text-right bold font20">$ 0.00</div>
		</div>
	</div>
	<div class="span1"></div>
</div>
<?php
if($estado!=8){
?>
<div><button type="submit" name="send" value="ok" class="btn btn-success">Guardar y <?php echo $estado==2?'cotizar':'facturar' ?> ODS</button></div>
<?php } ?>
</form>
<script type="text/javascript">
	calcularNeto(__TOTALNETO);
</script>
