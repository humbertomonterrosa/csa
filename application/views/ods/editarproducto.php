<form method="post" class="confirm-form" data-msg="¿Desea guardar los cambios realizados?">
<h2>Edición de producto para la orden <?php echo 'ODS'.$idOrden; ?></h2>
<?php if(isset($err)){ echo '<div class="sep10 bold text-error">'.$err.'</div>';} ?>
<div class="title_form sep10">
	Datos del producto
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Marca Electrodomestico (*):</div>
		<input type="text" id="marca" name="marca" class="fill_parent" value="<?php echo $marca?>">
		<div class="text-error mar-5"><?php echo form_error('marca');?></div>
		<?php
		$arr = array();
		foreach ($mrcpdt as $key => $value) {
			$arr[] = $value->descripcion;
		}
		?>
		<script type="text/javascript">
			$("#marca").typeahead({
									source:<?php echo json_encode($arr);?>
			});
		</script>
	</div>
	<div class="span3">
		<div class="bold">Clase Electrodomestico (*):</div>
		<input type="text" id="clase" name="clase" class="fill_parent" value="<?php echo $clase;?>">
		<div class="text-error mar-5"><?php echo form_error('clase');?></div>
		<?php
		$arr = array();
		foreach ($clspdt as $key => $value) {
			$arr[] = $value->descripcion;
		}
		?>
		<script type="text/javascript">
			$("#clase").typeahead({
									source:<?php echo json_encode($arr);?>
			});
		</script>
	</div>
	<div class="span2">
		<div class="bold">Modelo (*):</div>
		<input type="text" id="modelo" name="modelo" class="fill_parent" value="<?php echo $modelo;?>">
		<script type="text/javascript">
			$("#modelo").typeahead({
									source:function(typeahead,query){
										return $.ajax({url:base_url+"suggest/modelo/",type:"POST",dataType:"json",data:"modelo="+typeahead,success:function(data){return query(data);}});
									}
			});
		</script>
		<div class="text-error mar-5"><?php echo form_error('modelo');?></div>
	</div>
	<div class="span4">
		<div class="bold">No. Serie (*):</div>
		<input type="text" name="serie" class="" value="<?php echo $serie;?>">
		<div class="text-error mar-5"><?php echo form_error('serie');?></div>
	</div>
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Falla (*):</div>
		<textarea name="falla" class="fill_parent"><?php echo $falla;?></textarea>
		<div class="text-error mar-5"><?php echo form_error('falla');?></div>
	</div>
	<div class="span3">
		<div class="bold">Estado físico:</div>
		<textarea name="estadofisico" class="fill_parent"><?php echo $estadofisico;?></textarea>
		<div class="text-error mar-5"><?php echo form_error('estadofisico');?></div>
	</div>
	<div class="span6">
		<div class="bold">Accesorios:</div>
		<textarea name="accesorios" class="span11"><?php echo $accesorios;?></textarea>
	</div>
</div>
<div class="sep10">
	<input type="submit" name="send" class="btn btn-success" value="Guardar"> <a href="<?php echo base_url('ods/ver/ODS'.$idOrden); ?>" class="btn">Cancelar</a>
</div>
</form>