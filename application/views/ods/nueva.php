<h2>Nueva Orden de Servicio</h2>
<form action="" method="post">
<div class="row-fluid">
	<div class="pull-right">
		<div class="control-group">
			<div class="controls">
				<div class="input-prepend input-append">
					<span class="add-on">Fecha Entrada: </span><input type="text" id="fentrada" name="fentrada" class="span4" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" value="<?php echo date('d-m-Y');?>" /> <span class="add-on"><a id="handler_datepicker" href="#"><li class="icon-calendar"></li></a></span>
					<script type="text/javascript">
					$("#fentrada").datepicker().on('changeDate',function(){
						$(this).datepicker('hide');
						$(this).blur();
					});

					$("#handler_datepicker").click(function(){
						$("#fentrada").datepicker('show');
						return false;
					});
					</script>
				</div>
			</div>
		</div>
	</div>
</div>
<p class="text-info"><b>Advertencia:</b> Los campos que contengan un (*) asterisco son obligatorios y se no se pueden dejar vacios.</p>
<div class="title_form">
	Información de la empresa o cliente
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Nit/Cedula (*):</div>
		<input type="text" id="ident" name="ident" class="fill_parent" value="<?php echo set_value('ident'); ?>">
		<div class="text-error mar-5"><?php echo form_error('ident');?></div>
		<script type="text/javascript">
		$("#ident").typeahead({
							source:function(typeahead,query){
								return $.ajax({url:base_url+"suggest/ident/"+typeahead,type:"GET",dataType:"json",success:function(data){return query(data);}});
							},
							minLength:3,
							updater:function(item){
								var part = item.split(" ");
								var ident = part[0];
								$.ajax({url:base_url+"suggest/cliente/"+ident,type:"GET",dataType:"json",success:function(data){
									var arr = {
												nombre:data.nombre,
												ciudad:data.ciudad,
												direccion:data.direccion,
												telefono:data.telefono,
												celular:data.celular
											 };
									set_val(arr);
									$("#marca").focus();
								}});
								return ident;
							}
						}
		);
		</script>
	</div>
	<div class="span5">
		<div class="bold">Empresa/Cliente (*):</div>
		<input id="nombre" type="text" name="nombre" class="fill_parent" value="<?php echo set_value('nombre'); ?>">
		<div class="text-error mar-5"><?php echo form_error('nombre');?></div>
	</div>
	<div class="span4">
		
	</div>
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Ciudad (*):</div>
		<input type="text" name="ciudad" class="fill_parent" value="<?php echo set_value('ciudad'); ?>">
		<div class="text-error mar-5"><?php echo form_error('ciudad');?></div>
	</div>
	<div class="span5">
		<div class="bold">Dirección (*):</div>
		<input type="text" name="direccion" class="fill_parent" value="<?php echo set_value('direccion'); ?>">
		<div class="text-error mar-5"><?php echo form_error('direccion');?></div>
	</div>
	<div class="span2">
		<div class="bold">Teléfono (*):</div>
		<input type="text" name="telefono" class="fill_parent" value="<?php echo set_value('telefono'); ?>">
		<div class="text-error mar-5"><?php echo form_error('telefono');?></div>
	</div>
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Celular:</div>
		<input type="text" name="celular" class="fill_parent" value="<?php echo set_value('celular'); ?>">
	</div>
</div>
<div class="title_form">
	Datos del producto
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Marca Electrodomestico (*):</div>
		<input type="text" id="marca" name="marca" class="fill_parent" value="<?php echo set_value('marca');?>">
		<div class="text-error mar-5"><?php echo form_error('marca');?></div>
		<?php
		$arr = array();
		foreach ($mrcpdt as $key => $value) {
			$arr[] = $value->descripcion;
		}
		?>
		<script type="text/javascript">
			$("#marca").typeahead({
									source:<?php echo json_encode($arr);?>
			});
		</script>
	</div>
	<div class="span3">
		<div class="bold">Clase Electrodomestico (*):</div>
		<input type="text" id="clase" name="clase" class="fill_parent" value="<?php echo set_value('clase');?>">
		<div class="text-error mar-5"><?php echo form_error('clase');?></div>
		<?php
		$arr = array();
		foreach ($clspdt as $key => $value) {
			$arr[] = $value->descripcion;
		}
		?>
		<script type="text/javascript">
			$("#clase").typeahead({
									source:<?php echo json_encode($arr);?>
			});
		</script>
	</div>
	<div class="span2">
		<div class="bold">Modelo:</div>
		<input type="text" id="modelo" name="modelo" class="fill_parent" value="<?php echo set_value('modelo');?>">
		<script type="text/javascript">
			$("#modelo").typeahead({
									source:function(typeahead,query){
										return $.ajax({url:base_url+"suggest/modelo/",type:"POST",dataType:"json",data:"modelo="+typeahead,success:function(data){return query(data);}});
									}
			});
		</script>
		<div class="text-error mar-5"><?php echo form_error('modelo');?></div>
	</div>
	<div class="span4">
		<div class="bold">Serie:</div>
		<input type="text" name="serie" value="<?php echo set_value('serie');?>">
		<div class="text-error mar-5"><?php echo form_error('serie');?></div>
	</div>
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Falla (*):</div>
		<textarea name="falla" class="fill_parent"><?php echo set_value('falla');?></textarea>
		<div class="text-error mar-5"><?php echo form_error('falla');?></div>
	</div>
	<div class="span3">
		<div class="bold">Estado físico:</div>
		<textarea name="estadofisico" class="fill_parent"><?php echo set_value('estadofisico');?></textarea>
		<div class="text-error mar-5"><?php echo form_error('estadofisico');?></div>
	</div>
	<div class="span6">
		<div class="bold">Accesorios:</div>
		<textarea name="accesorios" class="span11"><?php echo set_value('accesorios');?></textarea>
	</div>
</div>
<div class="title_form">
	Información de servicio 
</div>
<div class="row-fluid">
	<div class="span3">
		<div class="bold">Prestación de servicio:</div>
		<button id="btn_ps" type="button" data-target="tiposervicio" class="btn btn-primary">Particular</button>
		<input id="tiposervicio" type="hidden" name="tiposervicio" value="P">
	</div>
	<div class="span9">
		<div class="bold">Abono:</div>
		<div class="control-group">
			<div class="controls">
				<div class="input-prepend">
					<span class="add-on">$</span><input type="text" id="abono" name="abono" class="span2" value="<?php echo set_value('abono');?>" />
				</div>
			</div>
		</div>
		<div class="text-error mar-5"><?php echo form_error('abono');?></div>
	</div>
</div>
<div class="row-fluid">
	<div class="span6">
		<div class="bold">Observaciones:</div>
		<textarea name="observaciones" class="fill_parent"><?php echo set_value('observaciones');?></textarea>
	</div>
	<div class="span6">
	</div>
</div>
<div class="sep10">
	<input type="submit" class="btn btn-success" name="send" value="Guardar" /> <a href="<?php echo base_url('ods');?>" class="btn">Cancelar</a>
</div>
</form>