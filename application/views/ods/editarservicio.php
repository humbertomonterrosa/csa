<form method="post" class="confirm-form" data-msg="¿Desea guardar los cambios realizados?">
<h2>Edición del servicio para la orden <?php echo 'ODS'.$idOrden; ?></h2>
<?php if(isset($err)){ echo '<div class="sep10 bold text-error">'.$err.'</div>';} ?>
<div class="title_form sep10">
	Información de servicio
</div>
<div class="row-fluid">
	<div class="row-fluid">
	<div class="span3">
		<div class="bold">Prestación de servicio:</div>
		<button id="btn_ps" type="button" data-target="tiposervicio" class="btn btn-primary">Particular</button>
		<input id="tiposervicio" type="hidden" name="tiposervicio" value="P">
	</div>
	<div class="span9">
		<div class="bold">Abono:</div>
		<div class="control-group">
			<div class="controls">
				<div class="input-prepend">
					<span class="add-on">$</span><input type="text" id="abono" name="abono" class="span2" value="<?php echo set_value('abono',$abono);?>" />
				</div>
			</div>
		</div>
		<div class="text-error mar-5"><?php echo form_error('abono');?></div>
	</div>
</div>
</div>
<div class="sep10">
	<input type="submit" name="send" class="btn btn-success" value="Guardar"> <a href="<?php echo base_url('ods/ver/ODS'.$idOrden); ?>" class="btn">Cancelar</a>
</div>
</form>
<script type="text/javascript">
	setServicio('<?php echo set_value("tiposervicio",$tipo); ?>','<?php echo $abono; ?>');
</script>