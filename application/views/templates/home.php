<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php
	$user = $this->usuario->get(uid());
?>
<div class="est est-one">
	<div class="est-title">Información de usuario</div>
	<div class="est-content">
		<div class="row-fluid">
			<div class="span6 bold">
				Nombre:
			</div>
			<div class="span6">
				<?php echo $user->nombre." ".$user->apellido;?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6 bold">
				Nombre de usuario:
			</div>
			<div class="span6">
				<?php echo $user->username;?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6 bold">
				Tipo de usuario:
			</div>
			<div class="span6">
				<?php echo tipoUsuario($user->nivel);?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6 bold">
				Ip de conexión:
			</div>
			<div class="span6">
				<?php echo $this->session->userdata('ip_address');?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6 bold">
				Actividad:
			</div>
			<div class="span6">
				<?php echo date("d/m/Y H:i:s",$this->session->userdata('last_activity'));?>
			</div>
		</div>
	</div>
</div>

<!-- <div class="est est-one">
	<div class="est-title est-info">Estadisticas de servicios </div>
	<div class="est-content">
		Hello world
	</div>
</div>
 -->
<div class="est est-one">
	<div class="est-title est-danger">Ordenes pendietes por diagnostico <a href="<?php echo base_url('ods/buscarpor/ODS STATUS 1/ASC//1');?>"><span class="icon-share icon-white"></span></a></div>
	<div class="est-content est-content-margin1">
		<table class="table">
			<thead>
				<tr>
					<th width="5%">ODS</th>
					<th width="15%">Fecha</th>
					<th width="80%">Producto</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($pxd as $key => $value) {
				echo '<tr>';
				echo '<td><a href="'.base_url('ods/ver/ODS'.$value->idOrden).'">ODS'.$value->idOrden.'</a></td><td>'.date("d/m/Y",strtotime($value->fechaentrada)).'</td><td>'.$value->clase.' '.$value->marca.' '.$value->modelo.'</td>';
				echo '</tr>';
				if($key==4) break;
				} ?>
			</tbody>
		</table>
	</div>
</div>


<div class="est est-one">
	<div class="est-title est-info">Reparados pendientes por entregar <a href="<?php echo base_url('ods/buscar/ODS STATUS 6');?>"><span class="icon-share icon-white"></span></a></div>
	<div class="est-content est-content-margin1">
		<table class="table">
			<thead>
				<tr>
					<th width="5%">ODS</th>
					<th width="15%">Fecha</th>
					<th width="80%">Producto</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($rep as $key => $value) {
				echo '<tr>';
				echo '<td><a href="'.base_url('ods/ver/ODS'.$value->idOrden).'">ODS'.$value->idOrden.'</a></td><td>'.date("d/m/Y",strtotime($value->fechaentrada)).'</td><td>'.$value->clase.' '.$value->marca.' '.$value->modelo.'</td>';
				echo '</tr>';
				if($key==4) break;
				} ?>
			</tbody>
		</table>
	</div>
</div>

<div class="est est-middle">
	<div class="est-title est-info">Ordenes iniciadas recientemen</div>
	<div class="est-content est-content-margin1">
		<table class="table">
			<thead>
				<tr>
					<th width="5%">ODS</th>
					<th width="15%">Fecha</th>
					<th width="40%">Cliente</th>
					<th width="40%">Producto</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($inicializadas as $key => $value) {
				echo '<tr>';
				echo '<td><a href="'.base_url('ods/ver/ODS'.$value->idOrden).'">ODS'.$value->idOrden.'</a></td><td>'.date("d/m/Y",strtotime($value->fechaentrada)).'</td><td><a class="show-hand" data-placement="right" data-content="<strong>Teléfono:</strong> '.$value->telefono.'<br /><strong>Celular:</strong> '.$value->celular.'<br /><strong>Dirección:</strong> '.$value->direccion.', '.$value->ciudad.'<br /> <a href=\''.base_url('ods/buscar/'.$value->identificacion).'\'>Mas ODS de este cliente</a>" data-original-title="Información del cliente" rel="popover">'.$value->nombre.'</a></td><td>'.$value->clase.' '.$value->marca.' '.$value->modelo.'</td>';
				echo '</tr>';
				if($key==6) break;
				} ?>
			</tbody>
		</table>
	</div>
</div>

<div class="est est-middle">
	<div class="est-title est-danger">Servicios con mas de 15 días <a href="<?php echo base_url('ods/listarpordias/15'); ?>"><span class="icon-share icon-white"></span></a></div>
	<div class="est-content est-content-margin1">
		<table class="table">
			<thead>
				<tr>
					<th width="5%">ODS</th>
					<th width="10%">Fecha</th>
					<th width="40%">Cliente</th>
					<th width="40%">Producto</th>
					<th width="5%">Días</th>

				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($om15d as $key => $value) {
				echo '<tr>';
				echo '<td><a href="'.base_url('ods/ver/ODS'.$value->idOrden).'">ODS'.$value->idOrden.'</a></td><td>'.date("d/m/y",strtotime($value->fechaentrada)).'</td><td><a class="show-hand" data-placement="right" data-content="<strong>Teléfono:</strong> '.$value->telefono.'<br /><strong>Celular:</strong> '.$value->celular.'<br /><strong>Dirección:</strong> '.$value->direccion.', '.$value->ciudad.'<br /> <a href=\''.base_url('ods/buscar/'.$value->identificacion).'\'>Mas ODS de este cliente</a>" data-original-title="Información del cliente" rel="popover">'.$value->nombre.'</a></td><td>'.$value->clase.' '.$value->marca.' '.$value->modelo.'</td><td>'.$value->ndias.'</td>';
				echo '</tr>';
				if($key==6) break;
				} ?>
			</tbody>
		</table>
	</div>
</div>

<div class="est est-one">
	<div class="est-title est-info">Pendientes por repuesto <a href="<?php echo base_url('ods/buscar/ODS STATUS 7');?>"><span class="icon-share icon-white"></span></a></div>
	<div class="est-content est-content-margin1">
		<table class="table">
			<thead>
				<tr>
					<th width="5%">ODS</th>
					<th width="15%">Fecha</th>
					<th width="80%">Producto</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($pxr as $key => $value) {
				echo '<tr>';
				echo '<td><a href="'.base_url('ods/ver/ODS'.$value->idOrden).'">ODS'.$value->idOrden.'</a></td><td>'.date("d/m/Y",strtotime($value->fechaentrada)).'</td><td>'.$value->clase.' '.$value->marca.' '.$value->modelo.'</td>';
				echo '</tr>';
				if($key==4) break;
				} ?>
			</tbody>
		</table>
	</div>
</div>
