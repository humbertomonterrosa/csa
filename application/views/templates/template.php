<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title><?php echo $this->config->item("app_name").$title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Software aplicativo Clinica Digital">
    <meta name="author" content="Humberto Monterrosa">
    
    <!-- Le styles -->
    <link href="<?php echo base_url('css/bootstrap.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('css/bootstrap-responsive.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('css/custom.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('css/datepicker.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('css/jquery.dataTables_themeroller.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('css/jquery.fancybox.css'); ?>">

    <style type="text/css">
      body{
        background-color:<?php echo $this->config->item("ui_style_background"); ?>;
      }
    </style>
    <script type="text/javascript">
    var base_url ="<?php echo base_url();?>";
    </script>
    <script type="text/javascript" src="<?php echo base_url('js/jquery.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/bootstrap-button.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/bootstrap-typeahead.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/bootstrap-tooltip.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/bootstrap-popover.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/bootstrap-dropdown.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/bootstrap-modal.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/jquery.shortcuts.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/custom.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/bootstrap-datepicker.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/jquery.dataTables.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/jquery.fancybox.js');?>"></script>
  </head>

  <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand visible-phone" href="#"><img src="<?php echo base_url('img/logo.png');?>" width="28"></a>
          <a class="brand hidden-phone" href="#"><img src="<?php echo base_url('img/logo.png');?>" width="28"> <?php echo $this->config->item("app_name"); ?></a>
          <?php echo $this->load->view('templates/nav',true);?>
        </ul>
        </div>
      </div>
    </div>
    <div class="container">
      <?php echo $content;?>
      <div class="sep10 font12"><i class="bold">V <?php echo $this->config->item("app_version"); ?></i>, Admin: <?php echo $this->config->item("app_admin_email"); ?></div>
    </div> <!-- /container -->
  </body>
</html>
