<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if(uid()){
	?>
	<ul class="nav">
		<li><a href="<?php echo base_url();?>"><span class="icon-home icon-white"></span> Inicio</a></li>
		<li><a href="<?php echo base_url('ods');?>"><span class="icon-list-alt icon-white"></span> ODS</a></li>
		<?php
			if(isuser('ADMIN')){
				?>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Administración <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url('admin/marcapdt'); ?>"><span class="icon-th-large"></span> Marcas de Producto</a></li>
						<li><a href="<?php echo base_url('admin/clasepdt'); ?>"><span class="icon-th"></span> Clases de Producto</a></li>
						<li><a href="<?php echo base_url('admin/clientes'); ?>"><span class="icon-thumbs-up"></span> Clientes</a></li>
						<li><a href="<?php echo base_url('admin/usuarios'); ?>"><span class="icon-user"></span> Usuarios</a></li>
						<li><a href="<?php echo base_url('admin/backup'); ?>"><span class="icon-hdd"></span> Backup</a></li>
					</ul>
				</li>		
				<?php
			}
		?>
	</ul>
	<ul class="nav pull-right">
          <li class="dropdown">
          	<?php $user = $this->usuario->get(uid());?>
          	<a href="#" class="dropdown-toggle visible-phone" data-toggle="dropdown"><span class="icon-user icon-white"></span> <?php echo $user->username;?> <b class="caret"></b></a>
          	<a href="#" class="dropdown-toggle hidden-phone" data-toggle="dropdown"><span class="icon-user icon-white"></span> <?php echo $user->nombre.' '.$user->apellido;?> <b class="caret"></b></a>
          	<ul class="dropdown-menu">
          		<li><a href="<?php echo base_url('account/changepassword'); ?>">Cambiar contraseña</a></li>
          		<li class="divider"></li>
          		<li><a href="<?php echo base_url('account/logout'); ?>"><span class="icon-off"></span> Cerrar sesión</a></li>
          	</ul>
          </li>
	<?php
}
?>