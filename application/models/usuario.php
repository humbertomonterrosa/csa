<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getall(){
		$query = $this->db->query("SELECT * from usuario");
		return $query->result();
	}

	public function agregar($nombre,$apellido,$username,$passwd,$nivel){
		$nombre = $this->db->escape($nombre);
		$apellido = $this->db->escape($apellido);
		$username = $this->db->escape($username);
		$passwd = md5($passwd);		
		$query = $this->db->query("INSERT INTO Usuario VALUES(null,$nombre,$apellido,$username,'$passwd',$nivel,1)");
		return $this->db->insert_id();
	}

	public function editar($idUsuario,$nombre,$apellido,$nivel,$estado){
		$nombre = $this->db->escape($nombre);
		$apellido = $this->db->escape($apellido);		
		$query = $this->db->query("UPDATE Usuario SET nombre=$nombre,apellido=$apellido,nivel=$nivel,estado=$estado WHERE idUsuario=$idUsuario");
		return true;
	}

	public function get($idUsuario){
		$query = $this->db->query("SELECT * from Usuario WHERE idUsuario=$idUsuario");
		return $query->first_row();
	}

	public function getUsername($username){
		$username = $this->db->escape($username);
		$query = $this->db->query("SELECT * from Usuario WHERE username=$username");
		return $query->first_row();
	}

	public function autentificar($username,$passwd){
		$username = $this->db->escape($username);
		$passwd = $this->db->escape(md5($passwd));
		$query = $this->db->query("SELECT * from Usuario WHERE username=$username and passwd=$passwd and estado=1");
		$res = $query->result();
		if(count($res)==0) return false;
		else{
			$this->iniciarSesion($res[0]);
			return true;
		}
	}

	private function iniciarSesion($d){
		$this->session->set_userdata('idUsuario',$d->idUsuario);
		$this->session->set_userdata('access_time',time());		
	}

	public function changepassword($uid,$passwd){
		$this->db->query("UPDATE Usuario SET passwd=md5('$passwd') where idUsuario=$uid");
		
	}

	public function listar(){
		$query = $this->db->query("SELECT * FROM Usuario");
		return $query->result();
	}

	public function actualizar($idUsuario,$nombre,$apellido,$nivel){
		$nombre = $this->db->escape($nombre);
		$apellido = $this->db->escape($apellido);
		$this->db->query("UPDATE Usuario SET nombre=$nombre,apellido=$apellido,nivel=$nivel WHERE idUsuario=$idUsuario");
	}

	public function setEstado($idUsuario,$estado){
		$this->db->query("UPDATE Usuario SET estado=$estado WHERE idUsuario=$idUsuario");
	}

	public function resetpassword($idUsuario){
		$passwd = $this->config->item("sys_default_user_password");
		$this->db->query("UPDATE Usuario SET passwd=md5('$passwd') WHERE idUsuario=$idUsuario");
	}

}
?>
