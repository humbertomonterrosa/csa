<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Factura extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getAll(){
		$query = $this->db->query("SELECT * FROM Factura");
		return $query->result();
	}

	public function agregar($idOrden,$totaltrabajo,$totalpiezas,$descuento){
		$query = $this->db->query("INSERT INTO Factura VALUES(null,$idOrden,$totaltrabajo,$totalpiezas,$descuento,now())");
		return $this->db->insert_id();
	}

	public function editar($idFactura,$totaltrabajo,$totalpiezas,$descuento){
		$query = $this->db->query("UPDATE Factura SET totaltrabajo=$totaltrabajo,totalpiezas=$totalpiezas,descuento=$descuento,fecha=now() WHERE idFactura=$idFactura");
		return true;
	}

	public function get($idFactura){
		$query = $this->db->query("SELECT * FROM Factura WHERE idFactura=$idFactura");
		return $query->first_row();
	}

	public function getByOrden($idOrden){
		$query = $this->db->query("SELECT * FROM Factura WHERE idOrden=$idOrden");
		return $query->first_row();
	}
}

?>