<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Historial extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function get($idHistorial){
		$query = $this->db->query("SELECT * FROM Historial WHERE idHistorial=$idHistorial");
		return $query->result();
	}

	public function getByODS($ODS){
		$query = $this->db->query("SELECT Historial.*,concat(Usuario.nombre,' ',Usuario.apellido) as nombre FROM Historial,Usuario WHERE Usuario.idUsuario=Historial.idUsuario and concat('ODS',idOrden)='$ODS' ORDER BY idHistorial DESC");
		return $query->result();
	}

	public function setMensaje($idOrden,$mensaje){
		$idUsuario = uid();
		$this->db->query("INSERT INTO Historial VALUES(null,$idOrden,$idUsuario,'$mensaje',now())");
	}

}

/* End of file historial.php */
/* Location: ./application/models/historial.php */