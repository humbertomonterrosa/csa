<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Producto extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function get($pdt){
		$sql = $this->db->query("SELECT * FROM Producto WHERE idProducto=$pdt");
		return $sql->result();
	}

	public function getPdt($modelo,$idMarcaProducto,$serie,$idClaseProducto){
		$modelo = $this->db->escape($modelo);
		$serie = $this->db->escape($serie);
		$sql = $this->db->query("SELECT * FROM Producto WHERE modelo=$modelo AND idMarcaProducto=$idMarcaProducto AND serie=$serie AND idClaseProducto=$idClaseProducto");
		return $sql->first_row();
	}

	public function agregar($modelo,$idMarcaProducto,$serie,$idClaseProducto){
		$modelo = $this->db->escape($modelo);
		$serie = $this->db->escape($serie);
		$this->db->query("INSERT INTO Producto VALUES(null,$modelo,$idMarcaProducto,$serie,$idClaseProducto)");
		return $this->db->insert_id();

	}

	public function getModelos(){
		$sql = $this->db->query("SELECT * FROM Producto group by modelo");
		return $sql->result();
	}

	public function filtrarModelos($query){
		$sql = $this->db->query("SELECT * FROM Producto WHERE modelo LIKE '%$query%' group by modelo");
		return $sql->result();
	}

	public function editar($idProducto,$marca,$clase,$modelo,$serie){
		$modelo = $this->db->escape($modelo);
		$serie = $this->db->escape($serie);
		$this->db->query("UPDATE Producto SET idMarcaProducto=$marca,idClaseProducto=$clase,modelo=$modelo,serie=$serie WHERE idProducto=$idProducto");
	}
}
?>