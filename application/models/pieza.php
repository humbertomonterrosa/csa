<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pieza extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getAll(){
		$query = $this->db->query("SELECT * FROM Pieza");
		return $query->result();
	}

	public function agregar($descripcion,$valor){
		$descripcion = $this->db->escape($descripcion);
		$query = $this->db->query("INSERT INTO Pieza VALUES(null,$descripcion,$valor)");
		return $this->db->insert_id();
	}

	public function editar($idPieza,$descripcion,$valor){
		$descripcion = $this->db->escape($descripcion);
		$query = $this->db->query("UPDATE Pieza SET descripcion=$descripcion,valor=$valor WHERE idPieza=$idPieza");
		return true;
	}

	public function get($idPieza){
		$query = $this->db->query("SELECT * FROM Pieza WHERE idPieza=$idPieza");
		return $query->first_row();
	}

	public function getByDesc($descripcion){
		$descripcion = $this->db->escape($descripcion);
		$query = $this->db->query("SELECT * FROM Pieza WHERE descripcion=$descripcion");
		return $query->first_row();
	}

	public function filtrarByDesc($descripcion){
		$query = $this->db->query("SELECT * FROM Pieza WHERE descripcion like '%$descripcion%'");
		return $query->result();			
	}
}

?>