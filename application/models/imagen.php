<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imagen extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function get($idImagen){
		$query = $this->db->query("SELECT * FROM Imagen WHERE idImagen=$idImagen");
		return $query->first_row();
	}

	public function getByODS($ODS){
		$query = $this->db->query("SELECT * FROM Imagen WHERE concat('ODS',idOrden)='$ODS'");
		return $query->result();
	}

	public function agregar($idOrden,$archivo){
		$this->db->query("INSERT INTO Imagen VALUES(null,$idOrden,'$archivo')");
	}

	public function eliminar($idImagen,$archivo){
		$query = $this->db->query("DELETE FROM Imagen WHERE idImagen=$idImagen");
		unlink('uploads/'.$archivo);
	}

}

/* End of file imagen.php */
/* Location: ./application/models/imagen.php */