<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orden extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function listarshort(){
		$sql = $this->db->query("SELECT Orden.*,Producto.modelo,Producto.serie,MarcaProducto.descripcion as marca,ClaseProducto.descripcion as clase,Cliente.* FROM Orden,Producto,MarcaProducto,ClaseProducto,Cliente WHERE Producto.idProducto=Orden.idProducto and MarcaProducto.idMarcaProducto=Producto.idMarcaProducto and ClaseProducto.idClaseProducto=Producto.idClaseProducto and Cliente.idCliente=Orden.idCliente and Orden.estado!=0 ORDER BY idOrden DESC");
		return $sql->result();
	}

	public function get($ods){
		$sql = $this->db->query("SELECT Orden.*,Producto.modelo,Producto.serie,MarcaProducto.descripcion as marca,ClaseProducto.descripcion as clase,Cliente.*,nombretecnico,nombretecnico as tecnico,descuento,totaltrabajo,totalpiezas FROM Orden,Producto,MarcaProducto,ClaseProducto,Cliente,(SELECT IF(count(*)=0,'',nombre) as nombretecnico FROM Orden,Tecnico WHERE Tecnico.idTecnico=Orden.idTecnico and concat('ODS',Orden.idOrden)='$ods') as tec,(SELECT if(count(*)=0,'NULL','NOT NULL'),Factura.* FROM Factura WHERE concat('ODS',idOrden)='$ods') as fac WHERE Producto.idProducto=Orden.idProducto and MarcaProducto.idMarcaProducto=Producto.idMarcaProducto and ClaseProducto.idClaseProducto=Producto.idClaseProducto and Cliente.idCliente=Orden.idCliente and Orden.estado!=0 and concat('ODS',Orden.idOrden)='$ods'");
		return $sql->first_row();
	}

	public function getAnulada($ods){
		$sql = $this->db->query("SELECT Orden.*,Producto.modelo,Producto.serie,MarcaProducto.descripcion as marca,ClaseProducto.descripcion as clase,Cliente.*,nombretecnico,nombretecnico as tecnico,descuento,totaltrabajo,totalpiezas FROM Orden,Producto,MarcaProducto,ClaseProducto,Cliente,(SELECT IF(count(*)=0,'',nombre) as nombretecnico FROM Orden,Tecnico WHERE Tecnico.idTecnico=Orden.idTecnico and concat('ODS',Orden.idOrden)='$ods') as tec,(SELECT if(count(*)=0,'NULL','NOT NULL'),Factura.* FROM Factura WHERE concat('ODS',idOrden)='$ods') as fac WHERE Producto.idProducto=Orden.idProducto and MarcaProducto.idMarcaProducto=Producto.idMarcaProducto and ClaseProducto.idClaseProducto=Producto.idClaseProducto and Cliente.idCliente=Orden.idCliente and Orden.estado=0 and concat('ODS',Orden.idOrden)='$ods'");
		return $sql->first_row();
	}

	public function getAnuladas(){
		$sql = $this->db->query("SELECT Orden.*,Producto.modelo,Producto.serie,MarcaProducto.descripcion as marca,ClaseProducto.descripcion as clase,Cliente.* FROM Orden,Producto,MarcaProducto,ClaseProducto,Cliente WHERE Producto.idProducto=Orden.idProducto and MarcaProducto.idMarcaProducto=Producto.idMarcaProducto and ClaseProducto.idClaseProducto=Producto.idClaseProducto and Cliente.idCliente=Orden.idCliente and Orden.estado=0 ORDER BY idOrden DESC");	
		return $sql->result();
	}

	public function agregar($fentrada,$tiposervicio,$abono,$observacion,$accesorios,$estadofisico,$falla,$idCliente,$idProducto){
		$fentrada = date("Y-m-d",strtotime($fentrada));
		$observacion = $this->db->escape($observacion);
		$accesorios = $this->db->escape($accesorios);
		$falla = $this->db->escape($falla);
		$estadofisico = $this->db->escape($estadofisico);
		$creador = uid();
		$this->db->query("INSERT INTO Orden(fechaentrada,tipo,abono,observacion,accesorios,estadofisico,falla,idCliente,idProducto,creador) VALUES('$fentrada','$tiposervicio',$abono,$observacion,$accesorios,$estadofisico,$falla,$idCliente,$idProducto,$creador)");
		return $this->db->insert_id();
	}

	public function setEstado($ODS,$est){
		$this->db->query("UPDATE Orden SET estado=$est WHERE concat('ODS',idOrden)='$ODS'");
	}

	public function setSolucion($ODS,$solucion){
		$solucion = $this->db->escape($solucion);
		$this->db->query("UPDATE Orden SET solucion=$solucion WHERE concat('ODS',idOrden)='$ODS'");
	}

	public function diagnosticar($ODS,$idTecnico,$diagnostico){
		$diagnostico = $this->db->escape($diagnostico);
		$ODS = $this->db->escape($ODS);
		$this->db->query("UPDATE Orden SET idTecnico=$idTecnico,diagnostico=$diagnostico,estado=2 WHERE concat('ODS',idOrden)=$ODS");
	}

	public function autorizar($ODS,$aut=true){
		$ODS = $this->db->escape($ODS);
		$aut = $aut?3:6;
		$this->db->query("UPDATE Orden SET estado=$aut WHERE concat('ODS',idOrden)=$ODS");
	}

	public function pxr($ODS,$rep){
		$ODS = $this->db->escape($ODS);
		$rep = $this->db->escape($rep);
		$this->db->query("UPDATE Orden SET pendienterepuesto=$rep,estado=7 WHERE concat('ODS',idOrden)=$ODS");
	}

	public function filtrorapido($query,$by="idOrden",$ordenar="DESC"){
		$by = ($by=="idOrden"||$by=="fechaentrada"||$by=="fechasalida")?$by:"idOrden";
		$ordenar = ($ordenar=="DESC"||$ordenar=="ASC")?$ordenar:"DESC";
		$sql = $this->db->query("SELECT Orden.*,Producto.modelo,Producto.serie,MarcaProducto.descripcion as marca,ClaseProducto.descripcion as clase,Cliente.* FROM Orden,Producto,MarcaProducto,ClaseProducto,Cliente WHERE (Producto.idProducto=Orden.idProducto and MarcaProducto.idMarcaProducto=Producto.idMarcaProducto and ClaseProducto.idClaseProducto=Producto.idClaseProducto and Cliente.idCliente=Orden.idCliente and Orden.estado!=0) and (concat(ClaseProducto.descripcion,' ',Producto.modelo) LIKE '%$query%' or concat(ClaseProducto.descripcion,' ',MarcaProducto.descripcion,' ',Producto.modelo) LIKE '%$query%' or concat('ODS',Orden.idOrden)='$query' or Producto.modelo LIKE '%$query%' or Cliente.nombre LIKE '%$query%' or Producto.serie LIKE '%$query' or Cliente.identificacion='$query' or concat(ClaseProducto.descripcion,' ',MarcaProducto.descripcion) LIKE '%$query%' or concat(ClaseProducto.descripcion,' de ',Cliente.nombre) LIKE '%$query%' or Cliente.telefono='$query' or Cliente.celular='$query' or concat('PID',' ',Producto.idProducto)='$query' or concat('ODS STATUS',' ',estado)='$query' or concat('ODS BY ',creador)='$query' or concat('ODS YEAR ',year(fechaentrada))='$query' or concat('ODS MARCA ',MarcaProducto.descripcion)='$query' or concat('ODS CLASE ',ClaseProducto.descripcion)='$query') ORDER BY $by $ordenar");
		return $sql->result();
	}

	public function ordenconndias($dias){
		$sql = $this->db->query("SELECT Orden.*,Producto.modelo,Producto.serie,MarcaProducto.descripcion as marca,ClaseProducto.descripcion as clase,Cliente.*,datediff(date(now()),fechaentrada) as ndias FROM Orden,Producto,MarcaProducto,ClaseProducto,Cliente WHERE (Producto.idProducto=Orden.idProducto and MarcaProducto.idMarcaProducto=Producto.idMarcaProducto and ClaseProducto.idClaseProducto=Producto.idClaseProducto and Cliente.idCliente=Orden.idCliente and Orden.estado!=0) and datediff(date(now()),fechaentrada)>=$dias and estado!=8 ORDER BY ndias DESC");
		return $sql->result();
	}

	public function observacion($ODS,$observacion){
		$observacion = $this->db->escape($observacion);
		$this->db->query("UPDATE Orden SET observacion=$observacion WHERE concat('ODS',idOrden)='$ODS'");
	}

	public function setFechaSalida($ODS,$fecha=false){
		if($fecha===false) $date = "date(now())";
		else $date = $fecha;

		$this->db->query("UPDATE Orden SET fechasalida=$date WHERE concat('ODS',idOrden)='$ODS'");
	}

	public function setFinalizador($ODS){
		$uid = uid();
		$this->db->query("UPDATE Orden SET finalizador=$uid WHERE concat('ODS',idOrden)='$ODS'");
	}

	public function setProducto($ODS,$idProducto){
		$this->db->query("UPDATE Orden SET idProducto=$idProducto WHERE concat('ODS',idOrden)='$ODS'");
	}

	public function setFallaEstadoAcc($ODS,$falla,$estadofisico,$accesorios){
		$falla = $this->db->escape($falla);
		$estadofisico = $this->db->escape($estadofisico);
		$accesorios = $this->db->escape($accesorios);
		$this->db->query("UPDATE Orden SET falla=$falla,estadofisico=$estadofisico,accesorios=$accesorios WHERE concat('ODS',idOrden)='$ODS'");
	}

	public function anular($ODS){
		$this->db->query("UPDATE Orden SET estado=0 WHERE concat('ODS',idOrden)='$ODS'");
	}

	public function getNextID(){
		$schema = $this->db->database;
		$query = $this->db->query("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_schema='$schema' and table_name='Orden'");
		$row = $query->first_row();
		return $row->AUTO_INCREMENT;
	}

	public function eliminar($idOrden){
		$this->db->query("DELETE FROM Orden WHERE idOrden=$idOrden");
	}

	public function restablecer($ODS){
		$this->db->query("UPDATE Orden SET estado=1 WHERE concat('ODS',idOrden)='$ODS'");
	}

	public function actualizarServicio($idOrden,$tipo,$abono){
		$this->db->query("UPDATE Orden SET tipo='$tipo',abono=$abono WHERE idOrden=$idOrden");
	}
}

?>