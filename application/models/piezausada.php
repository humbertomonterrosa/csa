<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Piezausada extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getAll(){
		$query = $this->db->query("SELECT * FROM PiezaUsada");
		return $query->result();
	}

	public function agregar($idOrden,$idPieza,$cantidad,$valor){
		$query = $this->db->query("INSERT INTO PiezaUsada VALUES(null,$idOrden,$idPieza,$cantidad,$valor)");
		return $this->db->insert_id();
	}

	public function editar($idPiezaUsada,$idOrden,$idPieza,$cantidad,$valor){
		$query = $this->db->query("UPDATE PiezaUsada SET idOrden=$idOrden,idPieza=$idPieza,cantidad=$cantidad,valor=$valor WHERE idPiezaUsada=$idPiezaUsada");
		return true;
	}

	public function get($idPiezaUsada){
		$query = $this->db->query("SELECT * FROM PiezaUsada WHERE idPiezaUsada=$idPiezaUsada");
		return $query->first_row();
	}

	public function getByOrden($idOrden){
		$query = $this->db->query("SELECT * FROM PiezaUsada WHERE idOrden=$idOrden");
		return $query->result();
	}

	public function getByPiezaOrden($idPieza,$idOrden){
		$query = $this->db->query("SELECT * FROM PiezaUsada WHERE idOrden=$idOrden and idPieza=$idPieza");
		return $query->first_row();	
	}

	public function eliminar($idPiezaUsada){
		$query = $this->db->query("update Factura,PiezaUsada set Factura.totalpiezas=Factura.totalpiezas-(PiezaUsada.cantidad*PiezaUsada.valor) where idPiezaUsada=$idPiezaUsada and Factura.idOrden=PiezaUsada.idOrden");
		$query = $this->db->query("DELETE FROM PiezaUsada WHERE idPiezaUsada=$idPiezaUsada");		
	}
}

?>