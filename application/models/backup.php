<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backup extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function make(){
		$tables = $this->db->list_tables();
		$arr = array();
		$filename = "BACKUP-APP-".date("Ymd_His").".cdb";
		$sql = "-- ".md5('compress-db')."\r\n\r\n";
		foreach ($tables as $key => $value) {
			$sql .= "INSERT INTO ".$value."(";
			$fields = $this->db->field_data($value);
			$tmp = array();
			foreach ($fields as $field) {
				$tmp[] = $field->name;
			}
			$sql .= implode(",", $tmp).") VALUES";

			$values = $this->db->query("SELECT * FROM $value");
			$values = $values->result();
			$tmp = array();
			foreach ($values as $val) {
				$tmp_f = array();
				foreach ($fields as $field) {
					$type = strtoupper($field->type);
					$dato = $val->{$field->name};
					$tmp_f[] = $type=="INT"||$type=="TINYINT"||$type=="SMALLINT"||$type=="MEDIUMINT"||$type=="BIGINT"||
								 $type=="DECIMAL"||$type=="FLOAT"||$type=="DOUBLE"||$type=="REAL"||$type=="BIT"?($dato==NULL?'null':$dato):"'".$dato."'";
				}
				$tmp[] = "(".implode(",",$tmp_f).")";
			}
			$sql .= implode(",",$tmp).";".(isset($tables[$key+1])?"\r\n--NEXT\r\n":"");
		}
		
		$encode = bzcompress($sql);
		$this->load->helper('download');
		force_download($filename,$encode);
	}

	public function leer($filename){
		$this->load->helper('file');
		$data = read_file($filename);
		echo bzdecompress($data);
	}

	public function install($filename){
		$this->load->helper('file');
		$data = read_file($filename);
		$decode = bzdecompress($data);

		if(($fline=explode("\r\n", $decode)[0])!="-- ".md5('compress-db')){
			throw new Exception("Archivo invalido");
		}
		else{
			$tablas = $this->db->list_tables();
			foreach ($tablas as $key => $value) {
				$this->db->query("TRUNCATE $value");
			}

			$data = explode("--NEXT", $decode);
			foreach ($data as $key => $value) {
				$this->db->query($value);
			}
		}
	}

}

/* End of file backup.php */
/* Location: ./application/models/backup.php */