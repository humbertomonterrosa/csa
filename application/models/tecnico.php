<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tecnico extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getAll(){
		$query = $this->db->query("SELECT * FROM Tecnico");
		return $query->result();
	}

	public function agregar($nombre){
		$nombre = $this->db->escape($nombre);
		$query = $this->db->query("INSERT INTO Tecnico VALUES(null,$nombre)");
		return $this->db->insert_id();
	}

	public function editar($idTecnico,$nombre){
		$nombre = $this->db->escape($nombre);
		$query = $this->db->query("UPDATE Tecnico SET nombre=$nombre WHERE idTecnico=$idTecnico");
		return true;
	}

	public function get($idTecnico){
		$query = $this->db->query("SELECT * FROM Tecnico WHERE idTecnico=$idTecnico");
		return $query->first_row();
	}

	public function getByNombre($nombre){
		$nombre = $this->db->escape($nombre);
		$query = $this->db->query("SELECT * FROM Tecnico WHERE nombre=$nombre");
		return $query->first_row();
	}

	public function filtrarByNombre($nombre){
		$query = $this->db->query("SELECT * FROM Tecnico WHERE nombre LIKE '%$nombre%'");
		return $query->result();
	}
}
?>