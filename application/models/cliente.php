<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cliente extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function listar(){
		$sql = $this->db->query("SELECT * FROM Cliente");
		return $sql->result();
	}
	public function get($cid){
		$sql = $this->db->query("SELECT * FROM Cliente WHERE idCliente=$cid");
		return $sql->first_row();
	}

	public function getByIdent($ident){
		$sql = $this->db->query("SELECT * FROM Cliente WHERE identificacion='$ident'");
		return $sql->first_row();	
	}

	public function filtrarByIdent($query){
		$sql = $this->db->query("SELECT * FROM Cliente WHERE identificacion LIKE '%$query%'");
		return $sql->result();
	}

	public function getFirst(){
		$sql = $this->db->query("SELECT * FROM Cliente");
		return $sql->first_row();		
	}

	public function agregar($ident,$nombre,$ciudad,$direccion,$telefono,$celular){
		$ident = $this->db->escape($ident);
		$nombre = $this->db->escape($nombre);
		$ciudad = $this->db->escape($ciudad);
		$direccion = $this->db->escape($direccion);
		$telefono = $this->db->escape($telefono);
		$celular = $this->db->escape($celular);
		$this->db->query("INSERT INTO Cliente VALUES(NULL,$ident,$nombre,$ciudad,$direccion,$telefono,$celular,md5(concat($ident,';',$nombre,';',$ciudad,';',$direccion,';',$telefono,';',$celular)))");
		return $this->db->insert_id();
	}

	public function actualizar($idCliente,$nombre,$ciudad,$direccion,$telefono,$celular){
		$nombre = $this->db->escape($nombre);
		$ciudad = $this->db->escape($ciudad);
		$direccion = $this->db->escape($direccion);
		$telefono = $this->db->escape($telefono);
		$celular = $this->db->escape($celular);
		$this->db->query("UPDATE Cliente SET nombre=$nombre,ciudad=$ciudad,direccion=$direccion,telefono=$telefono,celular=$celular,hash=md5(concat(identificacion,';',$nombre,';',$ciudad,';',$direccion,';',$telefono,';',$celular)) where idCliente=$idCliente");
	}

	public function setIdent($idCliente,$ident){
		$ident = $this->db->escape($ident);
		$this->db->query("UPDATE Cliente SET identificacion=$ident,hash=md5(concat($ident,';',nombre,';',ciudad,';',direccion,';',telefono,';',celular)) where idCliente=$idCliente");
	}
}

?>