<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Claseproducto extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function get($cpdt){
		$sql = $this->db->query("SELECT * FROM ClaseProducto WHERE idClaseProducto=$cpdt");
		return $sql->first_row();
	}

	public function listar(){
		$sql = $this->db->query("SELECT * FROM ClaseProducto ORDER BY descripcion ASC");
		return $sql->result();
	}

	public function getByDesc($desc){
		$desc = $this->db->escape($desc);
		$sql = $this->db->query("SELECT * FROM ClaseProducto WHERE descripcion=$desc");
		return $sql->first_row();
	}

	public function agregar($desc){
		$desc = $this->db->escape($desc);
		$this->db->query("INSERT INTO ClaseProducto VALUES(null,$desc)");
		return $this->db->insert_id();
	}

	public function editar($idClaseProducto,$desc){
		$desc = $this->db->escape($desc);
		$this->db->query("UPDATE ClaseProducto SET descripcion=$desc WHERE idClaseProducto=$idClaseProducto");
	}
}

?>