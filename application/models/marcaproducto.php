<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marcaproducto extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function get($mpdt){
		$sql = $this->db->query("SELECT * FROM MarcaProducto WHERE idMarcaProducto=$mpdt");
		return $sql->first_row();
	}

	public function listar(){
		$sql = $this->db->query("SELECT * FROM MarcaProducto ORDER BY descripcion ASC");
		return $sql->result();
	}

	public function getByDesc($desc){
		$desc = $this->db->escape($desc);
		$sql = $this->db->query("SELECT * FROM MarcaProducto WHERE descripcion=$desc");
		return $sql->first_row();
	}

	public function agregar($desc){
		$desc = $this->db->escape($desc);
		$this->db->query("INSERT INTO MarcaProducto VALUES(null,$desc)");
		return $this->db->insert_id();
	}

	public function editar($idMarcaProducto,$desc){
		$desc = $this->db->escape($desc);
		$this->db->query("UPDATE MarcaProducto SET descripcion=$desc WHERE idMarcaProducto=$idMarcaProducto");
	}
}

?>