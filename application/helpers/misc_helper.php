<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(! function_exists("uid")){
	function uid(){
		$CI =& get_instance();
		return $CI->session->userdata("idUsuario");
	}
}

if(! function_exists("onlogin")){
	function onlogin(){
		if(!uid()) redirect(base_url('account/login'));
	}
}

if(! function_exists("genCode")){
	function genCode($num=16){
		$r = "";
		for($i=0;$i<$num;$i++){
			$arr = array(array(48,57),array(65,90),array(97,122));
			$index = $arr[rand(0,2)];
			$chr = rand($index[0],$index[1]);
			$r.= chr($chr);
		}
		return $r;
	}
}

if(! function_exists("estadoOrden")){
	function estadoOrden($n,$abv=true){
		$estados = array(	
							array("PXD","Pendiente por diagnóstico"),
							array("DIAG","Diagnosticado"),
							array("AUT","Autorizado"),
							array("NAUT","No Autorizado"),
							array("DEV","Devolución"),
							array("REP","Reparado"),
							array("PXR","Pendiente por Repuesto"),
							array("ENT","Entregado"),
							array("REIN","Reingreso")
						);
		return isset($estados[$n-1])?($abv?$estados[$n-1][0]:$estados[$n-1][1]):false;
	}
}

if(! function_exists("deleteVoid")){
	function deleteVoid($arr){
		$tmp = array();

		foreach ($arr as $key => $value) {
			if($value!=null){
				$tmp[] = $value;
			}
		}

		return $tmp;
	}
}

if(! function_exists("tipoUsuario")){
	function tipoUsuario($nivel){
		$tipos = array("Administrador","Normal","Solo lectura");
		return isset($tipos[$nivel-1])?$tipos[$nivel-1]:false;
	}
}

if(! function_exists("getuser")){
	function getuser(){
		if(uid()){
		$CI =& get_instance();
		$user = $CI->usuario->get(uid());
		return $user;
		}
		return false;
	}
}

if(! function_exists("isuser")){
	function isuser($n){
		$n = strtoupper($n);
		$arr = array("ADMIN","NORMAL","RO");
		$user = getuser();
		if(isset($arr[$user->nivel-1])){
			return $arr[$user->nivel-1]==$n;
		}
		return false;
	}
}

if(! function_exists("getOrden")){
	function getOrden($ODS=0,$msg=false){
		$CI =& get_instance();
		$orden = $CI->orden->get($ODS);
		if(count($orden)==0){
			show_404();
			return false;
		}
		else{
			return $orden;
		}
	}
}

if(! function_exists("verificaMantenimiento")){
	function verificaMantenimiento(){
		if(file_exists("restaurar")){
			show_error('Plataforma en mantenimiento.');
		}
	}
}

if(! function_exists("odstemplate")){
	function odstemplate($titulo,$title_page,$view,$ddd){
		$CI =& get_instance();
		$dd["content"] = $CI->load->view($view,$ddd,true);
		$dd["page_title"] = $title_page;
		$d["title"] = $titulo;
		$d["content"] = $CI->load->view('ods/ods',$dd,true);
		$CI->load->view('templates/template',$d);
	}
}
?>