<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function __construct(){
		parent::__construct();
		verificaMantenimiento();
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	onlogin();
		$dd["inicializadas"] = $this->orden->filtrorapido('ODS BY '.uid());
		$dd["pxd"] = $this->orden->filtrorapido('ODS STATUS 1',null,"ASC");
		$dd["pxr"] = $this->orden->filtrorapido('ODS STATUS 7');
		$dd["rep"] = $this->orden->filtrorapido('ODS STATUS 6');
		$dd["om15d"] = $this->orden->ordenconndias(15);
		$d["title"] = "";
		$d["content"] = $this->load->view('templates/home',$dd,true);
		$this->load->view('templates/template',$d);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */