<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ods extends CI_Controller{

	public function __construct(){
		parent::__construct();
		verificaMantenimiento();
	}

	public function index(){
		onlogin();
		$ddd["ordenes"] = $this->orden->listarshort();
		$config['base_url'] = base_url('ods/page/');
		$config['total_rows'] = count($ddd["ordenes"]);
		$config['per_page'] = 40;
		$ddd["page"] = 0;
		$ddd["per_page"] = $config["per_page"];
		$this->pagination->initialize($config);
		$ddd["paginas"] = $this->pagination->create_links();
		
		odstemplate(" - ODS","ORDENES DE SERVICIO","ods/lista",$ddd);
	}

	public function page($pag=0){
		onlogin();
		$ddd["ordenes"] = $this->orden->listarshort();
		$config['base_url'] = base_url('ods/page/');
		$config['total_rows'] = count($ddd["ordenes"]);
		$config['per_page'] = 40;
		$ddd["page"] = $pag;
		$ddd["per_page"] = $config["per_page"];
		$this->pagination->initialize($config);
		$ddd["paginas"] = $this->pagination->create_links();
		odstemplate(" - ODS","ORDENES DE SERVICIO","ods/lista",$ddd);
	}

	public function nueva(){
		onlogin();

		$fentrada = $this->input->post('fentrada'); 
		$ident = $this->input->post('ident');
		$nombre = $this->input->post('nombre');
		$ciudad = $this->input->post('ciudad');
		$direccion = $this->input->post('direccion');
		$telefono = $this->input->post('telefono');
		$celular = $this->input->post('celular');
		$marca = $this->input->post('marca');
		$clase = $this->input->post('clase');
		$modelo = $this->input->post('modelo');
		$serie = $this->input->post('serie');
		$falla = $this->input->post('falla');
		$estadofisico = $this->input->post('estadofisico');
		$accesorios = $this->input->post('accesorios');
		$tiposervicio = $this->input->post('tiposervicio');
		$abono = $this->input->post('abono');
		$observaciones = $this->input->post('observaciones');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('ident','Identificación','callback_identvalida');
			$this->form_validation->set_rules('nombre','','required');
			$this->form_validation->set_rules('ciudad','','required');
			$this->form_validation->set_rules('direccion','','required');
			$this->form_validation->set_rules('telefono','','required');
			$this->form_validation->set_rules('marca','','required');
			$this->form_validation->set_rules('clase','','required');
			$this->form_validation->set_rules('modelo','','');
			$this->form_validation->set_rules('serie','','');
			$this->form_validation->set_rules('falla','','required');
			if($tiposervicio=="P"){
				$this->form_validation->set_rules('abono','','required|integer');
			}
			else{
				$abono = 0;
			}

			$this->form_validation->set_message('required','Campo requerido.');
			$this->form_validation->set_message('integer','Solo se admite números enteros.');

			if($this->form_validation->run()){
				$clt = $this->cliente->getByIdent($ident);
				if(count($clt)==0||$ident==null){
				$idCliente = $this->cliente->agregar($ident,$nombre,$ciudad,$direccion,$telefono,$celular);
				}
				else{
					$idCliente = $clt->idCliente;
					$hash = md5($ident.';'.$nombre.';'.$ciudad.';'.$direccion.';'.$telefono.';'.$celular);
					if($hash!=$clt->hash){
						$this->cliente->actualizar($idCliente,$nombre,$ciudad,$direccion,$telefono,$celular);
					}
			
				}

				$marca = strtoupper($marca);
				$mpdt = $this->marcaproducto->getByDesc($marca);
				if(count($mpdt)==0){
					$idMarcaProducto = $this->marcaproducto->agregar($marca);
				}
				else{
					$idMarcaProducto = $mpdt->idMarcaProducto;
				}

				$clase = strtoupper($clase);
				$cpdt = $this->claseproducto->getByDesc($clase);
				if(count($cpdt)==0){
					$idClaseProducto = $this->claseproducto->agregar($clase);
				}
				else{
					$idClaseProducto = $cpdt->idClaseProducto;
				}

				$idProducto = $this->producto->agregar($modelo,$idMarcaProducto,$serie,$idClaseProducto);
				
				$idOrden = $this->orden->agregar($fentrada,$tiposervicio,$abono,$observaciones,$accesorios,$estadofisico,$falla,$idCliente,$idProducto);
				$this->historial->setMensaje($idOrden,"creó la orden de servicio.");
				redirect(base_url('ods/ver/ODS'.$idOrden.'/saved'));

			}
		}

		$ddd["clspdt"] = $this->claseproducto->listar();
		$ddd["mrcpdt"] = $this->marcaproducto->listar();	
		odstemplate(" - Nueva Orden de Servicio","ORDENES DE SERVICIO","ods/nueva",$ddd);
	}

	public function identvalida($str){
		if(preg_match('/^(?:[0-9]{5,}(?:-[0-9]+)?)?$/', $str)){
			return true;
		}
		else{
			$this->form_validation->set_message('identvalida','Identificación invalida.');
			return false;
		}
	}

	public function listarpordias($ndias){
		onlogin();
		$ddd["ordenes"] = $this->orden->ordenconndias($ndias);
		$config['base_url'] = base_url('ods/page/');
		$config['total_rows'] = count($ddd["ordenes"]);
		$config['per_page'] = 40;
		$ddd["page"] = 0;
		$ddd["per_page"] = $config["per_page"];
		$this->pagination->initialize($config);
		$ddd["paginas"] = $this->pagination->create_links();
		odstemplate(" - ODS","ORDENES DE SERVICIO","ods/lista",$ddd);
	}

	public function odsprint($ODS=0){
		onlogin();
		$orden = getOrden($ODS);
		
		$this->load->view('ods/impresion',$orden);
	}

	public function remprint($ODS=0){
		onlogin();
		$orden = getOrden($ODS);

		$pzu = $this->piezausada->getByOrden($orden->idOrden);
		$orden->pzu = $pzu;
		
		$this->load->view('ods/impresionrem',$orden);
	}

	public function buscar($q=false,$pag=0,$ordenar="DESC",$by="idOrden"){
		onlogin();

		if(!$q){
			$q = $this->input->post('q');
			if($q){
				$q = str_replace("/", "", $q);
				$q = rawurlencode(htmlentities($q));
				redirect(base_url("ods/buscar/".$q));
			}
			else redirect(base_url('ods'));
		}
		else{
			$q = rawurldecode($q);
			$q = html_entity_decode($q);
			$q = addslashes($q);

			$ddd["q"] = $q;
			$ddd["ordenes"] = $this->orden->filtrorapido($q,$by,$ordenar);
			$config['base_url'] = base_url('ods/buscar/'.$q.'/'.$pag);
			$config['uri_segment'] = 4;
			$config['total_rows'] = count($ddd["ordenes"]);
			$config['per_page'] = 40;
			$ddd["page"] = $pag;
			$ddd["per_page"] = $config["per_page"];
			$this->pagination->initialize($config);
			$ddd["paginas"] = $this->pagination->create_links();
			odstemplate(" - Buscador","ORDENES DE SERVICIO","ods/lista",$ddd);
		}
	}

	public function buscarpor($q=false,$ordenar="DESC",$by="idOrden",$pag=0){
		onlogin();

		if(!$q){
			$q = $this->input->post('q');
			if($q){
				$q = str_replace("/", "", $q);
				$q = rawurlencode(htmlentities($q));
				redirect(base_url("ods/buscar/".$q));
			}
			else redirect(base_url('ods'));
		}
		else{
			$q = rawurldecode($q);
			$q = html_entity_decode($q);
			$q = addslashes($q);
			$ddd["q"] = $q;
			$ddd["ordenes"] = $this->orden->filtrorapido($q,$by,$ordenar);
			$config['base_url'] = base_url('ods/buscarpor/'.$q.'/'.$ordenar.'/'.$by.'/');
			$config['uri_segment'] = 6;
			$config['total_rows'] = count($ddd["ordenes"]);
			$config['per_page'] = 40;
			$ddd["page"] = $pag;
			$ddd["per_page"] = $config["per_page"];
			$this->pagination->initialize($config);
			$ddd["paginas"] = $this->pagination->create_links();
			odstemplate(" - Buscador","ORDENES DE SERVICIO","ods/lista",$ddd);
		}
	}

	public function ver($ODS=0,$saved=false){
		onlogin();
		$orden = getOrden($ODS);

		$orden->saved = !$saved?false:true;
		odstemplate(" - ORDEN ODS".$orden->idOrden,"ORDENES DE SERVICIO","ods/ver",$orden);
	}

	public function diagnosticar($ODS=0){
		onlogin();
		$orden = getOrden($ODS);

		if(!($orden->estado==1||$orden->estado==9)){
			show_error('La orden '.$ODS.' no se encuentra pendiente por diagnóstico.');
		}
		$diagnostico = $this->input->post('diagnostico');
		$tecnico = $this->input->post('tecnico');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('diagnostico','','required');
			$this->form_validation->set_rules('tecnico','','required');

			$this->form_validation->set_message('required','Campo requerido.');
			if($this->form_validation->run()){
				$tec = $this->tecnico->getByNombre($tecnico);
				if(count($tec)==0){
					$idTecnico = $this->tecnico->agregar(strtoupper($tecnico));
				}
				else{
					$idTecnico = $tec->idTecnico;
				}
				$this->orden->diagnosticar($ODS,$idTecnico,$diagnostico);
				$this->historial->setMensaje($orden->idOrden,"diagnosticó la orden de servicio.");
				redirect(base_url('ods/ver/'.$ODS.'#autorizacion'));
			}
		}

		$orden = getOrden($ODS);

		odstemplate(" - ORDEN ODS".$orden->idOrden,"ORDENES DE SERVICIO","ods/ver",$orden);
	}

	public function autorizacion($ODS=0){
		onlogin();
		$orden = getOrden($ODS);

		if($orden->estado!=2){
			show_error("Solo puede hacer autorizaciones justo antes despues de dar un diagnóstico técnico");
		}
		$op1 = $this->input->post('op1');
		$op2 = $this->input->post('op2');
		$op3 = $this->input->post('op3');

		if($op1) { $this->orden->setEstado($ODS,3); $this->historial->setMensaje($orden->idOrden,"cambia de estado a Autorizado a la orden de servicio.");}
		if($op2) { $this->orden->setEstado($ODS,4); $this->historial->setMensaje($orden->idOrden,"cambia de estado a No Autorizado a la orden de servicio."); $this->orden->setSolucion($ODS,'El cliente no autoriza la reparación'); }
		if($op3) { $this->orden->setEstado($ODS,5); $this->historial->setMensaje($orden->idOrden,"cambia de estado a Devolución a la orden de servicio."); $this->orden->setSolucion($ODS,'Devolución del producto'); }

		redirect(base_url('ods/ver/'.$ODS.'#pxr'));
	}

	public function pxr($ODS=-1){
		$array = array("success"=>false,"msg"=>"Error al guardar repuestos pendietes.");
		if(uid()){
			$orden = $this->orden->get($ODS);
			if(count($orden)!=0){
				$pxr = $this->input->post('pxr');

				if($pxr){
					$array["success"]=true;
					$array["msg"]="Repuestos pendientes guardados.";
					$this->historial->setMensaje($orden->idOrden,"actualizó los repuestos pendientes.");
					$this->orden->pxr($ODS,$pxr);
				}
			}
		}

		echo json_encode($array);;

	}

	public function observacion($ODS=0){
		$array = array("success"=>false,"msg"=>"Error al agregar la observación");
		$orden = $this->orden->get($ODS);
		if(count($orden)!=0){
			if(uid()){
				$observacion = $this->input->post('observacion');
				if($observacion){
					$this->orden->observacion($ODS,$observacion);
					$this->historial->setMensaje($orden->idOrden,"actualizó las observaciones.");
					$array["success"] = true;
					$array["msg"] = "Observación guardada.";
				}
			}
		}

		echo json_encode($array);
	}

	public function estadofinal($ODS=0){
		onlogin();
		$orden = getOrden($ODS);

		if(!($orden->estado==3||$orden->estado==7)){
			show_error("Proceso no autorizado.");
		}

		$solucion = $this->input->post('solucion');

		$op1 = $this->input->post('op1');
		$op2 = $this->input->post('op2');

		if($op1||$op2){
			$this->form_validation->set_rules('solucion','','required');

			if($this->form_validation->run()){
				if ($op1){ $this->orden->setEstado($ODS,6); $this->historial->setMensaje($orden->idOrden,"cambia de estado a Reparado a la orden de servicio."); }
				elseif ($op2){ $this->orden->setEstado($ODS,5); $this->historial->setMensaje($orden->idOrden,"cambia de estado a Devolución a la orden de servicio."); }

				$this->orden->setSolucion($ODS,$solucion);
				redirect(base_url('ods/ver/'.$ODS));
			}
		}

		odstemplate(" - ORDEN ODS".$orden->idOrden,"ORDENES DE SERVICIO","ods/ver",$orden);
	}

	public function facturar($ODS=0){
		onlogin();

		$orden = getOrden($ODS);

		if(!($orden->estado==2||$orden->estado==3||($orden->estado>=5&&$orden->estado<=8))){
			show_error("No puede facturar este producto");
		}

		$send = $this->input->post('send');
		$pieza = $this->input->post('pieza');
		$pieza = $pieza?deleteVoid($pieza):array();
		$cantidad = $this->input->post('cantidad');
		$cantidad = $cantidad?deleteVoid($cantidad):array();
		$precio = $this->input->post('precio');
		$precio = $precio?deleteVoid($precio):array();
		$totaltrabajo = $this->input->post('trabajo');
		$descuento = $this->input->post('descuento');

		$fac = $this->factura->getByOrden($orden->idOrden);
		$fac = count($fac)==0?false:$fac;
		
		$cpieza = count($pieza);
		$ccantidad = count($cantidad);
		$cvalor = count($precio);

		$totalpiezas = 0;

		if($send){

			$descuento=$descuento?$descuento:0;
			$this->form_validation->set_rules('trabajo','','required|integer');
			if($this->form_validation->run()){
				if($cpieza*3!=($cpieza+$ccantidad+$cvalor)){
					$orden->err = "Algo ha salido mal, intente ingresar nuevamente las piezas, verifique que ningun campo quede vacio.";
				}
				else{

					foreach ($cantidad as $key => $value) {
						$totalpiezas+=$value*$precio[$key];
					}

					foreach ($pieza as $key => $value) {
						$pz = $this->pieza->getByDesc($value);
						if(count($pz)==0){
							$idPieza = $this->pieza->agregar($value,$precio[$key]);
						}
						else{
							$idPieza = $pz->idPieza;
						}

						$pzu = $this->piezausada->getByPiezaOrden($idPieza,$orden->idOrden);
						if(count($pzu)==0){
							$this->piezausada->agregar($orden->idOrden,$idPieza,$cantidad[$key],$precio[$key]);
						}
						else{
							$this->piezausada->editar($pzu->idPiezaUsada,$orden->idOrden,$idPieza,$cantidad[$key],$precio[$key]);
						}
					}

					$fac = $this->factura->getByOrden($orden->idOrden);
					if(count($fac)==0){
						$this->factura->agregar($orden->idOrden,$totaltrabajo,$totalpiezas,$descuento);
						$this->historial->setMensaje($orden->idOrden,"factura a la orden de servicio.");
					}
					else{
						$this->factura->editar($fac->idFactura,$totaltrabajo,$totalpiezas,$descuento);
						$this->historial->setMensaje($orden->idOrden,"actualiza facturación de la orden de servicio.");
					}
				}
				
			}
		}

		$pzu = $this->piezausada->getByOrden($orden->idOrden);
		$tmppz = array();
		$tmpct = array();
		$tmpvl = array();
		$tmppzu= array();
		
		foreach ($pzu as $key => $value) {
			$mpz = $this->pieza->get($value->idPieza);
			
			$tmppzu[]= $value->idPiezaUsada;
			$tmppz[] = strtoupper($mpz->descripcion);
			$tmpct[] = $value->cantidad;
			$tmpvl[] = number_format($value->valor,0,"","");
		}

		$totaltrabajo = $totaltrabajo===false?($fac?number_format($fac->totaltrabajo,0,"",""):'0'):($totaltrabajo);
		$descuento = $descuento===false?($fac?number_format($fac->descuento,0,"",""):number_format($orden->abono,0,"","")):($descuento);

		$orden->factura = $fac;
		$orden->saved = false;
		$orden->npiezas = count($tmppz);
		$orden->idpzu = $tmppzu;
		$orden->pieza = $tmppz;
		$orden->cantidad = $tmpct;
		$orden->valor = $tmpvl;
		$orden->trabajo = $totaltrabajo;
		$orden->descuento = $descuento;
		odstemplate(" - ".($orden->estado==2?'COTIZACIÓN':'FACTURACIÓN')." ODS".$orden->idOrden,"ORDENES DE SERVICIO","ods/facturar",$orden);
	}

	public function eliminarpzu($idpzu){
		if(!uid()) echo json_encode(array());
		$this->piezausada->eliminar($idpzu);
	}

	public function entregar($ODS=0){
		onlogin();
		
		$entregar = $this->input->post('entregar');
		$orden = getOrden($ODS);
		if(!($orden->estado>=4&&$orden->estado<=6)){
			show_error('Aun no puede entregar este producto');
		}

		if($entregar){
			$this->orden->setEstado($ODS,8);
			$this->orden->setFechaSalida($ODS);
			$this->orden->setFinalizador($ODS);
			redirect(base_url('ods/ver/'.$ODS));
			$this->historial->setMensaje($orden->idOrden,"realiza la entrega del producto.");
		}
		$orden->factura = $this->factura->getByOrden($orden->idOrden);
		odstemplate(" - Entregar producto","ORDENES DE SERVICIO","ods/entrega",$orden);
	}

	public function editarproducto($ODS=0){
		onlogin();

		$orden = getOrden($ODS);

		if(!(isuser('ADMIN')||isuser('NORMAL')&&$orden->creador==uid()&&$orden->estado!=8)){
			show_error('Acceso restringido');
		}

		$marca = $this->input->post('marca');
		$clase = $this->input->post('clase');
		$modelo = $this->input->post('modelo');
		$serie = $this->input->post('serie');
		$falla = $this->input->post('falla');
		$estadofisico = $this->input->post('estadofisico');
		$accesorios = $this->input->post('accesorios');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('marca','Marca','required');
			$this->form_validation->set_rules('clase','Clase','required');
			$this->form_validation->set_rules('modelo','Modelo','required');
			$this->form_validation->set_rules('serie','Serie','required');
			$this->form_validation->set_rules('falla','Falla','required');

			$this->form_validation->set_message('required','Campo requerido.');

			if($this->form_validation->run()){
				
				$marca = strtoupper($marca);
				$mpdt = $this->marcaproducto->getByDesc($marca);
				if(count($mpdt)==0){
					$orden->err = "La marca \"$marca\" no existe.";
				}
				else{
					$idMarcaProducto = $mpdt->idMarcaProducto;
					
					$clase = strtoupper($clase);
					$cpdt = $this->claseproducto->getByDesc($clase);
					if(count($cpdt)==0){
						$orden->err = "La clase de producto \"$clase\" no existe.";
					}
					else{
						$idClaseProducto = $cpdt->idClaseProducto;
						$this->producto->editar($orden->idProducto,$idMarcaProducto,$idClaseProducto,$modelo,$serie);
						$this->orden->setFallaEstadoAcc($ODS,$falla,$estadofisico,$accesorios);
						$this->historial->setMensaje($orden->idOrden,"editó el producto.");
						redirect(base_url('ods/ver/'.$ODS));
					}
				}

				
			}
		}
		$orden->marca = !$marca?$orden->marca:$marca;
		$orden->clase = !$clase?$orden->clase:$clase;
		$orden->modelo = !$modelo?$orden->modelo:$modelo;
		$orden->serie = !$serie?$orden->serie:$serie;
		$orden->falla = !$falla?$orden->falla:$falla;
		$orden->estadofisico = !$estadofisico?$orden->estadofisico:$estadofisico;
		$orden->accesorios = !$accesorios?$orden->accesorios:$accesorios;
		$orden->clspdt = $this->claseproducto->listar();
		$orden->mrcpdt = $this->marcaproducto->listar();
		odstemplate(" - Editar producto de ODS".$orden->idOrden,"ORDENES DE SERVICIO","ods/editarproducto",$orden);

	}

	public function anular($ODS=0){
		onlogin();

		if(!isuser('ADMIN')){
			show_404();
		}

		$orden = getOrden($ODS);

		$confirm = $this->input->post('confirm');

		if($confirm){
			$this->orden->anular($ODS);
			$this->historial->setMensaje($orden->idOrden,"anuló la orden de servicio.");
			redirect(base_url('ods'));
		}

		$d["title_msg"] = "Anular ORDEN $ODS";
		$d["msg"] = "Está a punto de anular la orden $ODS, confirme que desea anularla.";
		$d["link_cancel"] = base_url("ods/ver/$ODS");
		odstemplate(" - Anular ORDEN $ODS","ORDENES DE SERVICIO","templates/confirm.php",$d);
	}

	public function reingreso($ODS=0){
		onlogin();
		$orden = getOrden($ODS);
		if($orden->estado!=8){
			show_error("No pudes hacer reingreso a esta orden");
		}
		$mensaje = $this->input->post('mensaje');
		$confirm = $this->input->post('confirm');

		if($confirm){
			$obs = $orden->observacion."\r\n";
			$obs.= "[ENTREGADO ".date("d/m/Y",strtotime($orden->fechasalida))."]\r\n";
			$obs.= "[REINGRESO ".date("d/m/Y")."]\r\n";
			$this->orden->observacion($ODS,$obs);
			$this->orden->setFechaSalida($ODS,"null");
			$this->orden->setEstado($ODS,9);
			$this->historial->setMensaje($orden->idOrden,"da reingreso a la orden de servicio.");
			redirect(base_url('ods/ver/'.$ODS));
		}

		$d["title_msg"] = "Reingreso para la orden $ODS";
		$d["msg"] = "Está a punto de hacer reingreso a la orden <b>$ODS</b>, confirme que desea darle reingreso a esta orden.";
		$d["link_cancel"] = base_url('ods/ver/'.$ODS);
		odstemplate(" - Reingreso de ORDEN $ODS","ORDENES DE SERVICIO","templates/confirm",$d);

	}

	public function historial($ODS=0){
		onlogin();
		$orden = getOrden($ODS);
		$historial = $this->historial->getByODS($ODS);
		$orden->historial = $historial;
		odstemplate(" - Historial de ORDEN $ODS","ORDENES DE SERVICIO","ods/historial",$orden);
	}

	public function anuladas(){
		onlogin();
		if(!isuser('ADMIN')) show_404();
		$anuladas = $this->orden->getAnuladas();
		$lista = array();
		$ddd["tablecol"] = array(
									array("key_title"=>"ODS","key"=>"ods","width"=>"100"),
									array("key_title"=>"Cliente","key"=>"cliente","width"=>"25%"),
									array("key_title"=>"Producto","key"=>"producto","width"=>"23%"),
									array("key_title"=>"Falla","key"=>"falla","width"=>"32%"),
									array("key_title"=>"","key"=>"opt")
							);
		foreach ($anuladas as $key => $value) {
			$ddd["lista"][] = array("ods"=>"ODS".$value->idOrden,"cliente"=>$value->nombre,"producto"=>$value->clase." ".$value->marca." ".$value->modelo,"falla"=>$value->falla,"opt"=>'<a href="'.base_url('ods/restablecerods/ODS'.$value->idOrden).'" class="btn btn-danger btn-mini" title="Restablecer orden"><span class="icon-ok-sign icon-white"></span></a>');
		}
		$ddd["titulo"] = "Ordenes anuladas";
		odstemplate(" - Ordenes anuladas","ADMINISTRACIÓN DE SISTEMA","admin/listar",$ddd);
	}

	public function restablecerods($ODS){
		onlogin();
		if(!isuser('ADMIN')) show_404();
		$anulada = $this->orden->getAnulada($ODS);
		if(count($anulada)==0) show_404();

		$confirm = $this->input->post('confirm');

		if($confirm){
			$this->orden->restablecer($ODS);
			$this->historial->setMensaje($anulada->idOrden,"restableció la orden de servicio");
			redirect(base_url('ods/ver/'.$ODS));
		}

		$d["title_msg"] = "Restablecimiento de orden de servicio";
		$d["msg"] = "Está a punto de restablecer la orden \"$ODS\", ¿Desea restablecer la orden de servicio?";
		$d["link_cancel"] = base_url("ods/anuladas");
		odstemplate(" - Restablecimiento de orden de servicio","ADMINISTRACIÓN DE SISTEMA","templates/confirm.php",$d);
	}

	public function imagenes($ODS){
		onlogin();
		$orden = getOrden($ODS);
		$this->load->model('imagen');
		$send = $this->input->post('send');

		if($send){
			$filename = "IMG_".date("Ymd_His");
			$config["upload_path"] = "./uploads/";
			$config['allowed_types'] = 'jpg|png';
			$config['file_name'] = $filename;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('imagen')){
				$orden->err = $this->upload->display_errors();
			}
			else{
				$data = $this->upload->data();
				$this->imagen->agregar($orden->idOrden,$data["file_name"]);
			}
		}
		$orden->lista = $this->imagen->getByODS($ODS);


		odstemplate(" - Imágenes del producto","ORDENES DE SERVICIO","ods/imagenes.php",$orden);
	}

	public function eliminarimagen($idImagen,$archivo){
		onlogin();
		$this->load->model('imagen');
		$imagen = $this->imagen->get($idImagen);
		if(count($imagen)==0) show_404();
		$orden = getOrden('ODS'.$imagen->idOrden);
		if(!(isuser('ADMIN')||$orden->creador==uid())) show_404();
		$this->imagen->eliminar($idImagen,$imagen->archivo);
		redirect(base_url('ods/imagenes/ODS'.$imagen->idOrden));
	}

	public function editarservicio($ODS){
		onlogin();
		$orden = getOrden($ODS);
		if(!(isuser('ADMIN')||isuser('NORMAL')&&$orden->creador==uid()&&$orden->estado!=8)){
			show_error('Acceso restringido');
		}
		$orden->abono = number_format($orden->abono,0,'','');
		$tiposervicio = $this->input->post('tiposervicio');
		$abono = $this->input->post('abono');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('tiposervicio','Tipo servicio','required');
			if($tiposervicio=="P"){
				$this->form_validation->set_rules('abono','Abono','required|integer');
			}
			else{
				$abono=0;
			}

			if($this->form_validation->run()){
				$this->orden->actualizarServicio($orden->idOrden,$tiposervicio,$abono);
				$this->historial->setMensaje($orden->idOrden,"actualizó infomación del servicio.");
				redirect(base_url('ods/ver/'.$ODS));
			}
		}

		odstemplate("- Editar servicio","ORDENES DE SERVICIO","ods/editarservicio",$orden);
	}
}

?>