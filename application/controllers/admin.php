<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		show_404();
	}

	public function marcapdt(){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}

		$marcas = $this->marcaproducto->listar();
		$lista = array();
		foreach ($marcas as $key => $value) {
			$num = count($this->orden->filtrorapido("ODS MARCA ".$value->descripcion));
			$lista[] = array("id"=>$value->idMarcaProducto,"descripcion"=>$value->descripcion,"badge"=>$num!=0?'<span class="badge badge-success">'.$num.'</span>':'<span class="badge">'.$num.'</span>',"opt"=>'<a href="'.base_url('admin/marcapdt_editar/'.$value->idMarcaProducto).'" class="btn btn-danger btn-mini"><li class="icon-edit icon-white"></li></a>');
		}
		$ddd["tablecol"] = array(
									array("key"=>"id","key_title"=>"ID"),
									array("key"=>"descripcion","key_title"=>"Descripción"),
									array("key"=>"badge","key_title"=>"ODS Asoc.","width"=>"100"),
									array("key"=>"opt","key_title"=>"","width"=>"70")
								);
		$ddd["lista"] = $lista;
		$ddd["titulo"] = "Marcas de Productos";
		$ddd["add_link"] = 'admin/marcapdt_agregar';
		$ddd["add_text"] = "Agregar Marca de producto";

		odstemplate(" - Marcas","ADMINISTRACIÓN DE SISTEMA","admin/listar",$ddd);
	}

	public function marcapdt_agregar(){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}

		$descripcion = $this->input->post('descripcion');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('descripcion','Descripcion','required');
			$this->form_validation->set_message('required','Campo requerido.');
			if($this->form_validation->run()){
				$descripcion = strtoupper($descripcion);
				$marca = $this->marcaproducto->getByDesc($descripcion);
				if(count($marca)==0){
					$this->marcaproducto->agregar($descripcion);
					redirect(base_url('admin/marcapdt'));
				}
				else{
					$ddd["err"] = "La marca \"$descripcion\" ya existe.";
				}
			}
		}
		
		$ddd["descripcion"] = $descripcion;
		$ddd["titulo"] = "Agregar marca de producto";
		$ddd["link_cancel"] = 'admin/marcapdt';

		odstemplate(" - Agregar marca de producto","ADMINISTRACIÓN DE SISTEMA","admin/form-new",$ddd);
	}

	public function marcapdt_editar($idMarcaProducto=-1){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}
		$mpdt = $this->marcaproducto->get($idMarcaProducto);
		if(count($mpdt)==0){
			show_404();
		}

		$descripcion = $this->input->post('descripcion');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('descripcion','Descripcion','required');
			$this->form_validation->set_message('required','Campo requerido.');
			if($this->form_validation->run()){
				$descripcion = strtoupper($descripcion);
				$marca = $this->marcaproducto->getByDesc($descripcion);
				if(count($marca)==0){
					$this->marcaproducto->editar($idMarcaProducto,$descripcion);
					redirect(base_url('admin/marcapdt'));
				}
				else{
					$ddd["err"] = "La marca \"$descripcion\" ya existe.";
				}
			}
		}
		
		$ddd["descripcion"] = !$descripcion?$mpdt->descripcion:$descripcion;
		$ddd["titulo"] = "Editar marca";
		$ddd["link_cancel"] = 'admin/marcapdt';

		odstemplate(" - Editar marca","ADMINISTRACIÓN DE SISTEMA","admin/form-new",$ddd);
	}

	public function clasepdt(){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}

		$marcas = $this->claseproducto->listar();
		$lista = array();
		foreach ($marcas as $key => $value) {
			$num = count($this->orden->filtrorapido("ODS CLASE ".$value->descripcion));
			$lista[] = array("id"=>$value->idClaseProducto,"descripcion"=>$value->descripcion,"badge"=>$num!=0?'<span class="badge badge-success">'.$num.'</span>':'<span class="badge">'.$num.'</span>',"opt"=>'<a href="'.base_url('admin/clasepdt_editar/'.$value->idClaseProducto).'" class="btn btn-danger btn-mini"><li class="icon-edit icon-white"></li></a>');
		}
		$ddd["tablecol"] = array(
									array("key"=>"id","key_title"=>"ID"),
									array("key"=>"descripcion","key_title"=>"Descripción"),
									array("key"=>"badge","key_title"=>"ODS Asoc.","width"=>"70"),
									array("key"=>"opt","key_title"=>"","width"=>"70")
								);
		$ddd["lista"] = $lista;
		$ddd["titulo"] = "Clases de producto";
		$ddd["add_link"] = 'admin/clasepdt_agregar';
		$ddd["add_text"] = "Agregar Clase de Producto";

		odstemplate(" - Clases de Producto","ADMINISTRACIÓN DE SISTEMA","admin/listar",$ddd);
	}

	public function clasepdt_agregar(){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}

		$descripcion = $this->input->post('descripcion');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('descripcion','Descripcion','required');
			$this->form_validation->set_message('required','Campo requerido.');
			if($this->form_validation->run()){
				$descripcion = strtoupper($descripcion);
				$marca = $this->claseproducto->getByDesc($descripcion);
				if(count($marca)==0){
					$this->claseproducto->agregar($descripcion);
					redirect(base_url('admin/clasepdt'));
				}
				else{
					$ddd["err"] = "La clase de producto \"$descripcion\" ya existe.";
				}
			}
		}
		
		$ddd["descripcion"] = $descripcion;
		$ddd["titulo"] = "Agregar clase de producto";
		$ddd["link_cancel"] = 'admin/clasepdt';

		odstemplate(" - Agregar clase de producto","ADMINISTRACIÓN DE SISTEMA","admin/form-new",$ddd);
	}

	public function clasepdt_editar($idClaseProducto=-1){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}
		$cpdt = $this->claseproducto->get($idClaseProducto);
		if(count($cpdt)==0){
			show_404();
		}

		$descripcion = $this->input->post('descripcion');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('descripcion','Descripcion','required');
			$this->form_validation->set_message('required','Campo requerido.');
			if($this->form_validation->run()){
				$descripcion = strtoupper($descripcion);
				$clase = $this->claseproducto->getByDesc($descripcion);
				if(count($clase)==0){
					$this->claseproducto->editar($idClaseProducto,$descripcion);
					redirect(base_url('admin/clasepdt'));
				}
				else{
					$ddd["err"] = "La clase de producto \"$descripcion\" ya existe.";
				}
			}
		}
		
		$ddd["descripcion"] = !$descripcion?$cpdt->descripcion:$descripcion;
		$ddd["titulo"] = "Editar clase de producto";
		$ddd["link_cancel"] = 'admin/clasepdt';

		odstemplate(" - Editar clase de producto","ADMINISTRACIÓN DE SISTEMA","admin/form-new",$ddd);
	}

	public function clientes(){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}

		$clientes = $this->cliente->listar();
		$lista = array();
		foreach ($clientes as $key => $value) {
			$lista[] = array("id"=>$value->idCliente,"nombre"=>$value->nombre,"direccion"=>$value->direccion.", ".$value->ciudad,"opt"=>'<a href="'.base_url('admin/cliente_editar/'.$value->idCliente).'" class="btn btn-danger btn-mini"><li class="icon-edit icon-white"></li></a>');
		}
		$ddd["tablecol"] = array(
									array("key"=>"nombre","key_title"=>"Nombre","width"=>"50%"),
									array("key"=>"direccion","key_title"=>"Dirección","width"=>"40%"),
									array("key"=>"opt","key_title"=>"")
								);
		$ddd["lista"] = $lista;
		$ddd["titulo"] = "Clientes";

		odstemplate(" - Clientes","ADMINISTRACIÓN DE SISTEMA","admin/listar",$ddd);
	}

	public function cliente_editar($idCliente=0){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}

		$cliente = $this->cliente->get($idCliente);
		if(count($cliente)==0){
			show_404();
		}
		
		$referer =  false;

		$ident = $this->input->post('ident');
		$nombre = $this->input->post('nombre');
		$ciudad = $this->input->post('ciudad');
		$direccion = $this->input->post('direccion');
		$telefono = $this->input->post('telefono');
		$celular = $this->input->post('celular');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('ident','Identificación','required|callback_identvalida');
			$this->form_validation->set_rules('nombre','Nombre','required');
			$this->form_validation->set_rules('ciudad','Ciudad','required');
			$this->form_validation->set_rules('direccion','Dirección','required');
			$this->form_validation->set_rules('telefono','Teléfono','required');
		
			if($this->form_validation->run()){
				if($cliente->identificacion!=$ident){
					$cli = $this->cliente->getByIdent($ident);
					if(count($cli)!=0){
						$nchange = true;
						$ddd["err"] = "Ya existe un cliente con la misma identificación";
					}
					else{
						$this->cliente->setIdent($cliente->idCliente,$ident);
					}
				}

				if(!isset($nchange)){
				$hash1 = md5($cliente->identificacion.";".$nombre.";".$ciudad.";".$direccion.";".$telefono.";".$celular);
				$hash2 = $cliente->hash;
				if($hash1!=$hash2){
					$this->cliente->actualizar($cliente->idCliente,$nombre,$ciudad,$direccion,$telefono,$celular);
				}
				}

				if(($referer=$this->session->userdata('referer'))){
					redirect($referer);
					$this->session->unset_userdata('referer');
				}
				else{
					redirect(base_url('admin/clientes'));
				}
			}
			
		}
		else{
			if(($referer=$this->input->server('HTTP_REFERER'))){
				$this->session->set_userdata('referer',$referer);
			}
			else{
				$this->session->unset_userdata('referer');
			}
		}
		
		$ddd["ident"] = !$ident?$cliente->identificacion:$ident;
		$ddd["nombre"] = !$nombre?$cliente->nombre:$nombre;
		$ddd["ciudad"] = !$ciudad?$cliente->ciudad:$ciudad;
		$ddd["direccion"] = !$direccion?$cliente->direccion:$direccion;
		$ddd["telefono"] = !$telefono?$cliente->telefono:$telefono;
		$ddd["celular"] = !$celular?$cliente->celular:$celular;

		$ddd["titulo"] = "Editar cliente";
		$ddd["link_cancel"] = $referer?$referer:base_url('admin/clientes');
		odstemplate(" - Editar cliente","ADMINISTRACIÓN DE SISTEMA","admin/form-cliente",$ddd);
	}

	public function identvalida($str){
		if(preg_match('/^[0-9]{5,}(?:-[0-9]+)?$/', $str)){
			return true;
		}
		else{
			$this->form_validation->set_message('identvalida','Identificación invalida.');
			return false;
		}
	}

	public function backup(){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}

		odstemplate(" - Backup","ADMINISTRACIÓN DE SISTEMA","admin/backup",array());
	}


	public function download_backup(){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}

		$this->load->model('backup');
		$this->backup->make();
	}

	public function restaurar($show=false){
		$data = array();
		if(!file_exists("restaurar")){
			onlogin();
			if(!isuser('ADMIN')){
				show_404();
			}
			$iniciar = $this->input->post('iniciar');
			if($iniciar){
				$this->load->helper('file');
				if(write_file('restaurar','')){
				redirect(base_url('admin/restaurar'));
				}
				else{
					$data["err"] = "Error al iniciar el proceso de restauración.";
				}
			}
		}
		else{
			$data["restaurando"] = "";

			$send = $this->input->post('send');

			$cancelar = $this->input->post('cancelar');

			if($send){
				$config["upload_path"] = "./";
				$config['allowed_types'] = 'cdb';

				$this->load->library('upload',$config);
				
				if(!$this->upload->do_upload('archivo')){
					$data["err"] = $this->upload->display_errors();
				}
				else{ 
					$this->load->model('backup');
					$up_data = $this->upload->data();
					$filename = $up_data["file_name"];
					if($show) {$this->backup->leer($filename); exit();}
					try{
					$this->backup->install($filename);
					$data["suc"] = "La restauración ha finalizado exitosamente.";
					unlink("restaurar");
					}
					catch(Exception $ex){
						$data["err"] = $ex->getMessage();
					}
					unlink($filename);

				}
			}
			if($cancelar){
				unlink("restaurar");
				redirect(base_url('admin/restaurar'));
			}

		}

		odstemplate(" - Restaurar","ADMINISTRACIÓN DE SISTEMA","admin/restaurar",$data);
	}

	public function backup_install($file){
		$this->load->model('backup');
		$this->backup->make();
	}

	public function usuarios(){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}
		$ddd["tablecol"] = array(
									array("key_title"=>"Username","key"=>"username","width"=>"20%"),
									array("key_title"=>"Nombre","key"=>"nombre","width"=>"60%"),
									array("key_title"=>"Estado","key"=>"estado","width"=>"10%"),
									array("key_title"=>"","key"=>"opt")
							);

		$users = $this->usuario->listar();
		foreach ($users as $key => $value) {
			$ddd["lista"][] = array("username"=>$value->username." (".($value->nivel==1?'Administrador':($value->nivel==2?'Normal':'Readonly')).")","nombre"=>$value->nombre." ".$value->apellido,"opt"=>'<a href="'.base_url('admin/usuario_editar/'.$value->username).'" class="btn btn-danger btn-mini"><li class="icon-edit icon-white"></li></a> '.(uid()!=$value->idUsuario?($value->estado==1?'<a href="'.base_url('admin/usuario_disable/'.$value->username).'" class="btn btn-danger btn-mini" title="Desactivar"><li class="icon-ban-circle icon-white"></li></a>':'<a href="'.base_url('admin/usuario_enable/'.$value->username).'" class="btn btn-danger btn-mini" title="Activar"><li class="icon-ok-sign icon-white"></li></a>').' <a href="'.base_url('admin/usuario_resetpassword/'.$value->username).'" class="btn btn-danger btn-mini" title="Resetear contraseña"><li class="icon-asterisk icon-white"></li></a>':''),"estado"=>($value->estado?'Activo':'Inactivo'));
		}
		$ddd["add_link"] = "admin/usuario_agregar";
		$ddd["add_text"] = "Agregar usuario";
		$ddd["titulo"] = "Usuarios";
		odstemplate(" - Usuarios","ADMINISTRACIÓN DE SISTEMA","admin/listar",$ddd);
	}

	public function usuario_agregar(){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}

		$nombre = $this->input->post('nombre');
		$apellido = $this->input->post('apellido');
		$username = $this->input->post('username');
		$nivel = $this->input->post('nivel');
		$passwd = $this->input->post('passwd');
		$repasswd = $this->input->post('repasswd');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('nombre','Nombre','required');
			$this->form_validation->set_rules('apellido','Apellido','required');
			$this->form_validation->set_rules('username','Nombre de usuario','required|min_length[4]|is_unique[Usuario.username]');
			$this->form_validation->set_rules('nivel','Nivel','required');
			$this->form_validation->set_rules('passwd','Contraseña','required|min_length[8]|matches[repasswd]');
			$this->form_validation->set_rules('repasswd','Contraseña de verificación','required|min_length[8]');
			$this->form_validation->set_message('required','Campo requerido.');
			$this->form_validation->set_message('min_length','%s requiere minimo %s caracteres.');
			$this->form_validation->set_message('matches','%s no coinciden con la %s.');
			$this->form_validation->set_message('is_unique','%s ya existe.');
			if($this->form_validation->run()){
				$this->usuario->agregar($nombre,$apellido,$username,$passwd,$nivel);
				redirect(base_url('admin/usuarios'));
			}
		}
		
		$ddd["nombre"] = $nombre;
		$ddd["apellido"] = $apellido;
		$ddd["username"] = $username;
		$ddd["nivel"] = $nivel;
		$ddd["passwd"] = $passwd;
		$ddd["repasswd"] = $repasswd;
		$ddd["titulo"] = "Agregar usuario";
		$ddd["link_cancel"] = base_url('admin/usuarios');
		odstemplate(" - Agregar usuario","ADMINISTRACIÓN DE SISTEMA","admin/form-usuario",$ddd);
	}

	public function usuario_editar($username=""){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}
		$user = $this->usuario->getUsername($username);
		if(count($user)==0){
			show_404();
		}

		$nombre = $this->input->post('nombre');
		$apellido = $this->input->post('apellido');
		$nivel = $this->input->post('nivel');
		$send = $this->input->post('send');

		if($send){
			$this->form_validation->set_rules('nombre','Nombre','required');
			$this->form_validation->set_rules('apellido','Apellido','required');
			$this->form_validation->set_rules('nivel','Nivel','required');

			if($this->form_validation->run()){
				$this->usuario->actualizar($user->idUsuario,$nombre,$apellido,$nivel);
				redirect(base_url("admin/usuarios"));
			}
		}
		
		
		$ddd["nombre"] = !$nombre?$user->nombre:$nombre;
		$ddd["apellido"] = !$apellido?$user->apellido:$apellido;
		$ddd["nivel"] = !$nivel?$user->nivel:$nivel;
		$ddd["titulo"] = "Editar usuario";
		$ddd["link_cancel"] = base_url("admin/usuarios");
		odstemplate(" - Editar marca","ADMINISTRACIÓN DE SISTEMA","admin/form-usuario",$ddd);
	}

	public function usuario_disable($username=""){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}
		$user = $this->usuario->getUsername($username);
		if(count($user)==0){
			show_404();
		}

		if(uid()==$user->idUsuario){
			redirect(base_url('admin/usuarios'));
		}

		$confirm = $this->input->post('confirm');
		if($confirm){
			$this->usuario->setEstado($user->idUsuario,0);
			redirect(base_url("admin/usuarios"));
		}

		$d["title_msg"] = "Desactivar cuenta de usuario";
		$d["msg"] = "Está a punto de desactivar la cuenta \"$username\", ¿Desea desactivar la cuenta?";
		$d["link_cancel"] = base_url("admin/usuarios");
		odstemplate(" - Desactivar cuenta","ADMINISTRACIÓN DE SISTEMA","templates/confirm.php",$d);
	}

	public function usuario_enable($username=""){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}
		$user = $this->usuario->getUsername($username);
		if(count($user)==0){
			show_404();
		}

		if(uid()==$user->idUsuario){
			redirect(base_url('admin/usuarios'));
		}
		

		$confirm = $this->input->post('confirm');
		if($confirm){
			$this->usuario->setEstado($user->idUsuario,1);
			redirect(base_url("admin/usuarios"));
		}


		$d["title_msg"] = "Activar cuenta de usuario";
		$d["msg"] = "Está a punto de activar la cuenta \"$username\", ¿Desea activar la cuenta?";
		$d["link_cancel"] = base_url("admin/usuarios");
		odstemplate(" - Activar cuenta","ADMINISTRACIÓN DE SISTEMA","templates/confirm.php",$d);
	}

	public function usuario_resetpassword($username=""){
		onlogin();
		if(!isuser('ADMIN')){
			show_404();
		}
		$user = $this->usuario->getUsername($username);
		if(count($user)==0){
			show_404();
		}

		$confirm = $this->input->post('confirm');
		if($confirm){
			$this->usuario->resetpassword($user->idUsuario);
			redirect(base_url('admin/usuarios'));
		}

		$passwd = $this->config->item("sys_default_user_password");
		$d["title_msg"] = "Resetear contraseña de usuario";
		$d["msg"] = "Está a punto de resetear contraseña de la cuenta \"$username\", su nueva contraseña sera <b>$passwd</b> ¿Desea resetear la contraseña?";
		$d["link_cancel"] = base_url("admin/usuarios");
		odstemplate(" - Resetear contraseña de usuario","ADMINISTRACIÓN DE SISTEMA","templates/confirm.php",$d);
	}

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */