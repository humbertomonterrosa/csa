<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suggest extends CI_Controller{

	public function ident($ident="-1"){
		if(!uid()) return json_encode(array());
		$clientes = $this->cliente->filtrarByIdent($ident);
		$arr = array();
		foreach ($clientes as $key => $value) {
			$arr[] = $value->identificacion." - ".$value->nombre;
		}
		echo json_encode($arr);
	}

	public function cliente($ident){
		if(!uid()) return json_encode(array());
		$cliente = $this->cliente->getByIdent($ident);
		echo json_encode($cliente);
	}

	public function modelo(){
		if(!uid()) return json_encode(array());
		$modelo = $this->input->post('modelo');
		if($modelo){
			$productos = $this->producto->filtrarModelos($modelo);
			$arr = array();
			foreach ($productos as $key => $value) {
				$arr[] = $value->modelo;
			}
			echo json_encode($arr);
		}
	}

	public function tecnico(){
		if(!uid()) return json_encode(array());
		$nombre = $this->input->post('nombre');
		if($nombre){
			$tecnicos = $this->tecnico->filtrarByNombre($nombre);
			$arr = array();
			foreach ($tecnicos as $key => $value) {
				$arr[]  = strtoupper($value->nombre);
			}
			echo json_encode($arr);
		}
	}

	public function pieza(){
		if(!uid()) {
			echo json_encode(array());
			return false;
		}
		$pieza = $this->input->post('pieza');
		if($pieza){
			$piezas = $this->pieza->filtrarByDesc($pieza);
			$arr = array();
			foreach ($piezas as $key => $value) {
				$arr[] = strtoupper($value->descripcion." - $ ".number_format($value->valor,0,"",""));
			}
			echo json_encode($arr);
			return true;
		}
	}
}
?>