<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller{

	public function login(){
		
		$user = $this->input->post('user');
		$passwd = $this->input->post('passwd');
		$send = $this->input->post('send');

		$dd = array();

		if($send){
			if(!($user&&$passwd)){
				$dd["err"] = "Debe ingresar su nombre de usuario y contraseña.";
			}
			else{
				if(!$this->usuario->autentificar($user,$passwd)){
					$dd["err"] = "Acceso invalido";
				}
				else{
					redirect(base_url());
				}
			}
		}

		$d["title"] = " - Login";
		$d["content"] = $this->load->view('usuario/login',$dd,true);
		$this->load->view('templates/template',$d);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('account/login'));
	}

	public function changepassword(){
		onlogin();

		$currentpasswd = $this->input->post('currentpasswd');
		$passwd = $this->input->post('passwd');
		$repasswd = $this->input->post('repasswd');
		$send = $this->input->post('send');
		$ddd = array();

		if($send){
			$this->form_validation->set_rules('currentpasswd','Contraseña actual','required');
			$this->form_validation->set_rules('passwd','Contraseña nueva','required|min_length[8]');
			$this->form_validation->set_rules('repasswd','Repita la contraseña nueva','required|min_length[8]');
			if(!$this->form_validation->run()){
				$ddd["err"] = validation_errors();
			}
			else{
				if($passwd!=$repasswd){
					$ddd["err"] = "Las contraseñas nuevas no coinciden.";
				}
				else{
					$user = $this->usuario->get(uid());	
					if($user->passwd!=md5($currentpasswd)){
						$ddd["err"] = "La contraseña actual no es valida.";
					}
					else{
						$this->usuario->changepassword(uid(),$passwd);
						redirect(base_url());
					}
				}
			}
		}

		odstemplate(" - Cambiar contraseña","ADMINISTRACIÓN DE CUENTA","usuario/changepassword",$ddd);
	}
}

?>